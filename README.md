# Description

This is a game in developpment. 
In this game, the world is totally generated procedurally.

# Directory description
* DelaunayVoronoi : implementation of algorithm which generate both graphs Voronoi and Delaunay (modify version of this [implementation](https://github.com/RafaelKuebler/DelaunayVoronoi))
* NoiseTester : a playground to test noises which are used in generation
* Project1 : Main project which contain the game
* SharpNoiseModule+ : some custom modules which work with [SharpNoise](https://github.com/rthome/SharpNoise) library
* Utility : some usefull method to manipulate object like array, ...
* UtilityTest : some unit test for utilisty method

