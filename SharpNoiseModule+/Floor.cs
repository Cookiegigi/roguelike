﻿using SharpNoise.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpNoiseModule_
{
    public class Floor : Module
    {
        public Floor() : base(1)
        {
        }

        public Module Source0 { get; set; }


        public override double GetValue(double x, double y, double z)
        {
            return Math.Floor(Source0.GetValue(x, y, z));
        }
    }
}
