﻿using SharpNoise.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpNoiseModule_
{
    public class Ceil : Module
    {
        public Ceil() : base(1)
        {
        }

        public Module Source0 { get; set; }


        public override double GetValue(double x, double y, double z)
        {
            return Math.Ceiling(Source0.GetValue(x, y, z));
        }
    }
}
