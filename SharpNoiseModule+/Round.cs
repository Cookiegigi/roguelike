﻿using SharpNoise.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpNoiseModule_
{
    public class Round : Module
    {
        public Round() : base(1)
        {
        }

        public Module Source0 { get; set; }


        public override double GetValue(double x, double y, double z)
        {
            return Math.Round(Source0.GetValue(x, y, z));
        }
    }
}
