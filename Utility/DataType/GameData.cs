﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Utility.DataType;

namespace Utility.DataType 
{
    public class GameData<T>
    {
        public string Path { get; set; }

        public SerializableDictionnary<string, T> Dictionnary { get; private set; }

        public GameData(string path) : this()
        {
            Path = path;

            if (!File.Exists(Path))
            {
                Save();
            }

            Load();
        }

        public GameData()
        {
            Dictionnary = new SerializableDictionnary<string, T>();
        }

        public void Save()
        {
            Dictionnary.Save(Path);
        }

        public void Load()
        {
            Dictionnary = SerializableDictionnary<string, T>.Load(Path);
        }

        public T this[string key]
        {
            get
            {
                return Dictionnary[key];
            }
            set
            {
                Dictionnary[key] = value;
            }
        }

        public string[] Labels
        {
            get
            {
                return Dictionnary.Keys.ToArray();
            }
        }
    }
}
