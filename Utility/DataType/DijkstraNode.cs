﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utility.DataType
{
    public class DijkstraNode
    {
        public DijkstraNode(double x, double y, int id)
        {
            X = x;
            Y = y;
            Id = id;
        }

        public int Id { get; set; }

        public double X { get; set; }
        public double Y { get; set; }

        public double Distance { get; set; }
        public DijkstraNode Previous { get; set; }

        public List<DijkstraNode> Neighbours { get; set; }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            if (!(obj is DijkstraNode)) return false;

            DijkstraNode temp = (DijkstraNode)obj;

            return temp.Id == Id;
        }

        public static double Length(DijkstraNode n1, DijkstraNode n2)
        {
            return Math.Sqrt((n1.X - n2.X) * (n1.X - n2.X) + (n1.Y - n2.Y) * (n1.Y - n2.Y));
        }
    }
}
