﻿using System;
using System.Collections;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Schema;
using System.IO;

namespace Utility.DataType
{
    public class SerializableDictionnary<TKey, TValue> : IXmlSerializable, IDictionary<TKey, TValue>
    {
        private Dictionary<TKey, TValue> dico;

        public SerializableDictionnary()
        {
            dico = new Dictionary<TKey, TValue>();
        }

        #region Interface Implementation

        public int Count
        {
            get
            {
                return dico.Count;
            }
        }

        public bool IsReadOnly
        {
            get
            {
                return false;
            }
        }

        public bool ContainsKey(TKey key)
        {
            return dico.ContainsKey(key);
        } 

        ICollection<TKey> IDictionary<TKey, TValue>.Keys
        {
            get
            {
                return dico.Keys;
            }
        }

        public ICollection<TKey> Keys
        {
            get
            {
                return dico.Keys;
            }
        }

        ICollection<TValue> IDictionary<TKey, TValue>.Values
        {
            get
            {
                return dico.Values;
            }
        }

        public ICollection<TValue> Values
        {
            get
            {
                return dico.Values;
            }
        }

        public TValue this[TKey key]
        { get => dico[key];
          set => dico[key] = value;
        }

        public void Clear()
        {
            dico.Clear();
        }

        public XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(XmlReader reader)
        {
            XmlSerializer xsKey = new XmlSerializer(typeof(TKey));
            XmlSerializer xsValue = new XmlSerializer(typeof(TValue));
            bool test = reader.IsEmptyElement;
            if (test)
            {
                return;
            }
            reader.Read();
            while(reader.NodeType != XmlNodeType.EndElement)
            {
                reader.ReadStartElement("item");
                reader.ReadStartElement("key");
                TKey key = (TKey)xsKey.Deserialize(reader);
                reader.ReadEndElement();
                reader.ReadStartElement("value");
                TValue value = (TValue)xsValue.Deserialize(reader);
                reader.ReadEndElement();
                this.Add(key, value);
                reader.ReadEndElement();
                reader.MoveToContent();
            }
            reader.ReadEndElement();

        }

        public void WriteXml(XmlWriter writer)
        {
            XmlSerializer xsKey = new XmlSerializer(typeof(TKey));
            XmlSerializer xsValue = new XmlSerializer(typeof(TValue));

            foreach (TKey key in dico.Keys)
            {
                writer.WriteStartElement("item");
                writer.WriteStartElement("key");
                xsKey.Serialize(writer, key);
                writer.WriteEndElement();
                writer.WriteStartElement("value");
                TValue value = this[key];
                xsValue.Serialize(writer, value);
                writer.WriteEndElement();
                writer.WriteEndElement();
            }
        }

        public void Add(TKey key, TValue value)
        {
            dico.Add(key, value);
        }

        public bool Remove(TKey key)
        {
            return dico.Remove(key);
        }

        public bool TryGetValue(TKey key, out TValue value)
        {
            return dico.TryGetValue(key, out value);
        }

        public void Add(KeyValuePair<TKey, TValue> item)
        {
            Add(item.Key, item.Value);
        }

        public bool Contains(KeyValuePair<TKey, TValue> item)
        {
            return dico.Contains(item);
        }

        public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
        {
            for(int i = arrayIndex; i < array.Length; i++)
            {
                Add(array[i]);
            }
        }

        public bool Remove(KeyValuePair<TKey, TValue> item)
        {
            return dico.Remove(item.Key);
        }

        IEnumerator<KeyValuePair<TKey, TValue>> IEnumerable<KeyValuePair<TKey, TValue>>.GetEnumerator()
        {
            return dico.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return dico.GetEnumerator();
        }

        #endregion

        public void Save(string path)
        {
            XmlSerializer xs = new XmlSerializer(typeof(SerializableDictionnary<TKey, TValue>));
            using(StreamWriter wr = new StreamWriter(path))
            {
                xs.Serialize(wr, this);
            }
        }

        public static SerializableDictionnary<TKey, TValue> Load(string path)
        {
            XmlSerializer xs = new XmlSerializer(typeof(SerializableDictionnary<TKey, TValue>));
            SerializableDictionnary<TKey, TValue> dico;
            using (StreamReader wr = new StreamReader(path))
            {
                dico = xs.Deserialize(wr) as SerializableDictionnary<TKey, TValue>;
            }
            return dico;
        }
    }
}
