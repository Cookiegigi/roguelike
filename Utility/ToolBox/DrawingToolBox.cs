﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utility.ToolBox
{
    public static class DrawingToolBox
    {
        public static Random RNG = new Random();

        /// <summary>
        /// Bresenham algorithm
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="array"></param>
        /// <param name="color"></param>
        /// <param name="p1"></param>
        /// <param name="p2"></param>
        public static void DrawLine(Color[,] array, Color color, int[] point1, int[] point2)
        {
            if (Math.Abs(point2[1] - point1[1]) < Math.Abs(point2[0] - point1[0]))
            {
                if (point1[0] > point2[0])
                {
                    DrawLineLow(array, color, point2, point1);
                }
                else
                {
                    DrawLineLow(array, color, point1, point2);
                }
            }
            else
            {
                if (point1[1] > point2[1])
                {
                    DrawLineHigh(array, color, point2, point1);
                }
                else
                {
                    DrawLineHigh(array, color, point1, point2);
                }
            }
        }

        private static void DrawLineLow(Color[,] array, Color color, int[] p1, int[] p2)
        {
            int dx = p2[0] - p1[0];
            int dy = p2[1] - p1[1];
            int yi = 1;

            if (dy < 0)
            {
                yi = -1;
                dy = -dy;
            }

            int D = 2 * dy - dx;
            int y = p1[1];

            for (int x = p1[0]; x <= p2[0]; x++)
            {
                if (x >= 0 && x < array.GetLength(0)
                    && y >= 0 && y < array.GetLength(1))
                {
                    array[x, y] = color;
                }


                if (D > 0)
                {
                    y += yi;
                    D -= 2 * dx;
                }

                D += 2 * dy;
            }
        }

        private static void DrawLineHigh(Color[,] array, Color color, int[] p1, int[] p2)
        {
            int dx = p2[0] - p1[0];
            int dy = p2[1] - p1[1];
            int xi = 1;

            if (dx < 0)
            {
                xi = -1;
                dx = -dx;
            }

            int D = 2 * dx - dy;
            int x = p1[0];

            for (int y = p1[1]; y <= p2[1]; y++)
            {
                if (x >= 0 && x < array.GetLength(0)
                    && y >= 0 && y < array.GetLength(1))
                {
                    array[x, y] = color;
                }

                if (D > 0)
                {
                    x += xi;
                    D -= 2 * dy;
                }

                D += 2 * dx;
            }
        }

        public static void DrawLineAntiAliasedThick(Color[,] array, Color color, int[] p1, int[] p2, double width)
        {

            DrawLineThickWithMultipleColor(array, new Color[] { color }, p1, p2, width, true);
        }

        public static void DrawLineThickWithMultipleColor(Color[,] array, Color[] colors, int[] p1
            , int[] p2, double width, bool antiAliased = false)
        {
            Color temp;

            //float r, g, b, a;

            int x0 = p1[0], x1 = p2[0], y0 = p1[1], y1 = p2[1];

            int dx = Math.Abs(x1 - x0); // |dx| : absolute value of differential on x
            int sx = x0 < x1 ? 1 : -1; // sign of differential on x
            int dy = Math.Abs(y1 - y0); // |dy| : absolute value of differential on y
            int sy = y0 < y1 ? 1 : -1; // sign of differential on y

            int err = dx - dy //error : exy =  dy(x + 1-x0) - dx (y+1-y0) + e_(n-1)
                , e2          // 
                , x2
                , y2;


            double ed = dx + dy == 0
            ? 1 : Math.Sqrt((double)dx * dx + (double)dy * dy); // maximum error value for the lien


            double wd = (width + 1) / 2;

            while (true)
            {
                temp = colors[RNG.Next(0, colors.Count())];

                if (x0 >= 0 && x0 < array.GetLength(0) &&
                    y0 >= 0 && y0 < array.GetLength(1))
                {
                    if (antiAliased)
                    {
                        array[x0, y0] = new Color(
                                        (float)Math.Max(0, temp.R * (Math.Abs(err - dx + dy) / ed - wd + 1)),
                                        (float)Math.Max(0, temp.G * (Math.Abs(err - dx + dy) / ed - wd + 1)),
                                        (float)Math.Max(0, temp.B * (Math.Abs(err - dx + dy) / ed - wd + 1)),
                                        temp.A
                                        );
                    }
                    else
                    {
                        array[x0, y0] = temp;
                    }
                }

                e2 = err;
                x2 = x0;

                if (2 * e2 >= -dx) // ex + exy > 0
                {
                    y2 = y0;
                    for (e2 += dy; e2 < ed * wd && (y1 != y2 || dx > dy); e2 += dx)
                    {
                        temp = colors[RNG.Next(0, colors.Count())];

                        y2 += sy;
                        if (x0 >= 0 && x0 < array.GetLength(0) &&
                            y2 >= 0 && y2 < array.GetLength(1))
                        {
                            if (antiAliased)
                            {
                                array[x0, y2] = new Color(
                                                (float)Math.Max(0, temp.R * (Math.Abs(err - dx + dy) / ed - wd + 1)),
                                                (float)Math.Max(0, temp.G * (Math.Abs(err - dx + dy) / ed - wd + 1)),
                                                (float)Math.Max(0, temp.B * (Math.Abs(err - dx + dy) / ed - wd + 1)),
                                                temp.A
                                                );
                            }
                            else
                            {
                                array[x0, y2] = temp;
                            }
                        }

                    }

                    if (x0 == x1) break; // stop condition
                    e2 = err;
                    err -= dy; // sub difference error
                    x0 += sx; // increment x in direction of gradient
                }
                if (2 * e2 <= dy) // ey + exy < 0
                {
                    for (e2 = dx - e2; e2 < ed * wd && (x1 != x2 || dx < dy); e2 += dy)
                    {
                        temp = colors[RNG.Next(0, colors.Count())];

                        x2 += sx;
                        if (x2 >= 0 && x2 < array.GetLength(0) &&
                            y0 >= 0 && y0 < array.GetLength(1))
                        {
                            if (antiAliased)
                            {
                                array[x2, y0] = new Color(
                                                (float)Math.Max(0, temp.R * (Math.Abs(err - dx + dy) / ed - wd + 1)),
                                                (float)Math.Max(0, temp.G * (Math.Abs(err - dx + dy) / ed - wd + 1)),
                                                (float)Math.Max(0, temp.B * (Math.Abs(err - dx + dy) / ed - wd + 1)),
                                                temp.A
                                                );
                            }
                            else
                            {
                                array[x2, y0] = temp;
                            }
                        }

                    }

                    if (y0 == y1) break; // stop condition
                    err += dx; // add difference error
                    y0 += sy; // increment y in direction of gradient
                }

            }
        }


        public static void DrawDisc<T>(T[,] array, T color, int[] center, int radius)
        {
            if (center[0] >= 0
                && center[0] < array.GetLength(0)
                && center[1] >= 0
                && center[1] < array.GetLength(1))
            {
                array[center[0], center[1]] = color;

                if (radius != 0)
                {
                    for (int i = 1; i <= radius; i++)
                    {
                        DrawCircle2(array, color, center, i);
                    }
                }
            }
        }

        /// <summary>
        /// Bresenham algorithm
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="array"></param>
        /// <param name="color"></param>
        /// <param name="center"></param>
        /// <param name="radius"></param>
        public static void DrawCircle1<T>(T[,] array, T color, int[] center, int radius)
        {
            //Exception handling
            if (center[0] - radius < 0
                || center[0] + radius > array.GetLength(0)
                || center[1] - radius < 0
                || center[1] + radius > array.GetLength(1))
            {
                throw new Exception($"Circle out of bounds " +
                    $"(center : ({center[0]}, {center[1]}) " +
                    $"and radius : {radius}");
            }


            int x = 0, y = radius, m = 5 - 4 * radius;

            while (x <= y)
            {
                array[x + center[0], y + center[1]] = color;
                array[y + center[0], x + center[1]] = color;
                array[-x + center[0], y + center[1]] = color;
                array[-y + center[0], x + center[1]] = color;
                array[x + center[0], -y + center[1]] = color;
                array[y + center[0], -x + center[1]] = color;
                array[-x + center[0], -y + center[1]] = color;
                array[-y + center[0], -x + center[1]] = color;

                if (m > 0)
                {
                    y -= 1;
                    m -= 8 * y;
                }

                x += 1;
                m += 8 * x + 4;
            }


        }

        /// <summary>
        /// Andres algorithm
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="array"></param>
        /// <param name="color"></param>
        /// <param name="center"></param>
        /// <param name="radius"></param>
        public static void DrawCircle2<T>(T[,] array, T color, int[] center, int radius)
        {
            //Exception handling
            if (!(center[0] - radius < 0
                || center[0] + radius > array.GetLength(0) - 1
                || center[1] - radius < 0
                || center[1] + radius > array.GetLength(1) - 1))
            {
                int x = 0, y = radius, d = radius - 1;

                while (y >= x)
                {
                    array[x + center[0], y + center[1]] = color;
                    array[y + center[0], x + center[1]] = color;
                    array[-x + center[0], y + center[1]] = color;
                    array[-y + center[0], x + center[1]] = color;
                    array[x + center[0], -y + center[1]] = color;
                    array[y + center[0], -x + center[1]] = color;
                    array[-x + center[0], -y + center[1]] = color;
                    array[-y + center[0], -x + center[1]] = color;

                    if (d > 2 * x)
                    {
                        d -= 2 * x + 1;
                        x += 1;
                    }
                    else if (d <= 2 * (radius - y))
                    {
                        d += 2 * y - 1;
                        y -= 1;
                    }
                    else
                    {
                        d += 2 * (y - x - 1);
                        y -= 1;
                        x += 1;
                    }
                }
            }


        }
    }
}
