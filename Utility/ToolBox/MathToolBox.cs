﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utility.ToolBox
{
    public static class MathToolBox
    {
        public static int[] GetSpiralCoord(int n)
        {
            int[] result = new int[2];

            //m-th blocks
            int m = (int)Math.Floor(Math.Sqrt(Convert.ToDouble(n)));
            double k;

            //pair
            if(m % 2 == 0)
            {
                if(n >= m * (m + 1))
                {
                    k = (double)m / 2;
                }
                else
                {
                    k = (double)m / 2 - 1;
                }
            }
            //impair
            else
            {
                k = (double)(m - 1) / 2;
            }


            if(2*k*(k+1) < n && n <= (2*k+1)* (2 * k + 1))
            {
                result[0] = (int)(n - 4 * k * k - 3 * k);
                result[1] = (int)(k);
            }
            else if((2 * k + 1) * (2 * k + 1) < n && n <= 2 * (k + 1) * (2 * k + 1))
            {
                result[0] = (int)(k+1);
                result[1] = (int)(4*k*k + 5*k+1 -n);
            }
            else if(2 * (k + 1) * (2 * k + 1) < n && n <= 4 * (k + 1) * (k + 1))
            {
                result[0] = (int)(4 * k * k + 7 * k +3 - n);
                result[1] = (int)(-k-1);
            }
            else if(4 * (k + 1) * (k + 1) < n && n <= 2 * (k + 1) * (2 * k + 3))
            {
                result[0] = (int)(-k - 1);
                result[1] = (int)(n - 4 * k * k - 9 * k - 5);
            }

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="array"></param>
        /// <param name="kernel">dimension must be odd</param>
        /// <returns></returns>
        public static double[,] ConvulutionProduct(double[,] array, double[,] kernel)
        {
            double[,] result = new double[array.GetLength(0), array.GetLength(1)];

            int kernelRadiusX = (int)Math.Round((double)(kernel.GetLength(0) - 1) / 2);
            int kernelRadiusY = (int)Math.Round((double)(kernel.GetLength(1) - 1) / 2);

            double temp;

            for (int i = 0
                ; i < array.GetLength(0); i++)
            {
                for (int j = 0
                    ; j < array.GetLength(1); j++)
                {
                    if(i >= kernelRadiusX 
                        && i < array.GetLength(0) - kernelRadiusX
                        && j >= kernelRadiusY
                        && j < array.GetLength(1) - kernelRadiusY
                        )
                    {
                        temp = 0;
                        for (int m = 0; m < kernel.GetLength(0); m++)
                        {
                            for (int n = 0; n < kernel.GetLength(1); n++)
                            {
                                temp += array[i + m - kernelRadiusX, j + n - kernelRadiusY] * kernel[m, n];
                            }
                        }

                        result[i, j] = temp;
                    }
                    else
                    {
                        result[i, j] = array[i, j];
                    }
                }
            }

            return result;
        }



    }
}
