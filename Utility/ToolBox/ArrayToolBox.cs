﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utility.ToolBox
{
    public static class ArrayToolBox
    {
        public static bool IsArrayFullOf(Type type, object[] array)
        {
            int c = 0;

            while (c < array.Length && (array[c] == null || type == array[c].GetType()))
            {
                c++;
            }

            return c == array.Length;
        }

        public static Type IsArrayFullWithUniqueType(object[] array)
        {
            int c = 0;

            while (c < array.Length && array[c] == null)
            {
                c++;
            }
            if (c != array.Length)
            {
                return IsArrayFullOf(array[c].GetType(), array) ? array[c].GetType() : null;
            }
            else
            {
                return null;
            }

        }

        public static int GetNumberOfNullItem(object[] array)
        {
            int result = 0;

            foreach (object obj in array)
            {
                if (obj == null)
                {
                    result++;
                }
            }

            return result;
        }

        public static int GetNumberofTypeItem(Type type, object[] array)
        {
            int result = 0;

            foreach (object obj in array)
            {
                if (obj != null && obj.GetType() == type)
                {
                    result++;
                }
            }

            return result;
        }

        public static object GetRandomItem(object[] array, Random rdn)
        {
            return array[rdn.Next(0, array.Length - 1)];
        }

        public static Type GetRandomItem(Type[] array, Random rdn)
        {
            return array[rdn.Next(0, array.Length - 1)];
        }

        public static Type[] GetTypes(object[] array)
        {
            HashSet<Type> result = new HashSet<Type>();

            foreach (object obj in array)
            {
                result.Add(obj.GetType());
            }

            return result.ToArray();
        }

        public static int[] GetOccurenceOfEachType(object[] array, out Type[] types)
        {
            List<int> result = new List<int>();
            List<Type> result2 = new List<Type>();

            Type temp;
            int index;

            foreach (object obj in array)
            {
                if (obj != null)
                {
                    temp = obj.GetType();
                    index = result2.IndexOf(temp);

                    if (index == -1)
                    {
                        result2.Add(temp);
                        result.Add(1);
                    }
                    else
                    {
                        result[index] += 1;
                    }
                }

            }


            types = result2.ToArray();
            return result.ToArray();
        }

        public static void GetOccurenceOfEachItem<T>(T[] array, out T[] Item, out int[] Count)
        {
            List<int> count = new List<int>();
            List<T> item = new List<T>();

            int index;

            foreach (T obj in array)
            {
                if (obj != null)
                {
                    index = item.IndexOf(obj);

                    if (index == -1)
                    {
                        item.Add(obj);
                        count.Add(1);
                    }
                    else
                    {
                        count[index] += 1;
                    }
                }
            }

            Item = item.ToArray();
            Count = count.ToArray();

        }

        public static double[] ConvertToDouble(int[] tab)
        {
            double[] result = new double[tab.Length];

            for (int i = 0; i < tab.Length; i++)
            {
                result[i] = (double)tab[i];
            }

            return result;
        }

        public static T[] Convert2DTo1D<T>(T[,] array)
        {
            T[] result = new T[array.GetLength(0) * array.GetLength(1)];

            for (int i = 0; i < array.GetLength(0); i++)
            {
                for (int j = 0; j < array.GetLength(1); j++)
                {
                    result[j * array.GetLength(0) + i] = array[i, j];
                }
            }

            return result;
        }

        //too slow
        public static T[,] FilterLowPass<T>(T[,] array)
        {
            T[,] result = new T[array.GetLength(0), array.GetLength(1)];

            List<T> temp = new List<T>();
            T[] item;
            int[] count;

            for (int i = 0; i < array.GetLength(0); i++)
            {
                for (int j = 0; j < array.GetLength(1); j++)
                {
                    temp.Add(array[i, j]);

                    if(i != 0 && j != 0)
                    {
                        temp.Add(array[i - 1, j - 1]);
                    }

                    if(i != 0)
                    {
                        temp.Add(array[i - 1, j]);
                    }

                    if(j != 0)
                    {
                        temp.Add(array[i, j - 1]);
                    }

                    if(i != array.GetLength(0) - 1 && j != array.GetLength(1) - 1)
                    {
                        temp.Add(array[i + 1, j + 1]);
                    }

                    if(i != array.GetLength(0) - 1)
                    {
                        temp.Add(array[i + 1, j]);
                    }

                    if(j != array.GetLength(1) - 1)
                    {
                        temp.Add(array[i, j + 1]);
                    }

                    if (i != array.GetLength(0) - 1 && j != 0)
                    {
                        temp.Add(array[i + 1, j - 1]);
                    }

                    if (i != 0 && j != array.GetLength(1) - 1)
                    {
                        temp.Add(array[i - 1, j + 1]);
                    }

                    GetOccurenceOfEachItem<T>(temp.ToArray(), out item, out count);

                    result[i, j] = item[count.ToList().IndexOf(count.Max())];
                }
            }

            return result;
        }

    }
}
