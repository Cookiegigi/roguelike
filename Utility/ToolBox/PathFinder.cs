﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility.DataType;

namespace Utility.ToolBox
{
    public class PathFinder
    {
        public List<DijkstraNode> Nodes { get; set; }

        public List<DijkstraNode> OpenNodes { get; set; }
        public List<DijkstraNode> CloseNodes { get; set; }

        public List<DijkstraNode> Path { get; set; }

        public PathFinder(List<DijkstraNode> nodes)
        {
            Nodes = nodes;

        }

        public void Process(DijkstraNode start, DijkstraNode stop = null)
        {
            Init(start);

            DijkstraNode temp;
            double distance;

            while (OpenNodes.Count != 0)
            {
                temp = OpenNodes.OrderBy(n => n.Distance).First();

                OpenNodes.Remove(temp);
                CloseNodes.Add(temp);

                if (stop != null && temp == stop)
                {
                    Path = new List<DijkstraNode>();
                    if (temp.Previous != null || temp == start)
                    {
                        while(temp != null)
                        {
                            Path.Add(temp);
                            temp = temp.Previous;
                        }
                    }


                    break;
                }

                foreach (DijkstraNode neighbour in temp.Neighbours.Where(n => OpenNodes.Contains(n)))
                {
                    distance = temp.Distance + DijkstraNode.Length(temp, neighbour);
                    if (distance < neighbour.Distance)
                    {
                        neighbour.Distance = distance;
                        neighbour.Previous = temp;
                    }
                }
            }

        }

        public void Init(DijkstraNode start)
        {

            OpenNodes = new List<DijkstraNode>();
            CloseNodes = new List<DijkstraNode>();

            foreach (DijkstraNode node in Nodes)
            {
                node.Distance = double.MaxValue;
                if (node == start)
                {
                    node.Distance = 0;
                }
                OpenNodes.Add(node);
            }
        }

    }
}
