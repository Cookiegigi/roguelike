﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Utility.ToolBox
{
    public static class StringToolBox
    {

        public static string GetOnlyUpperLetters(string value)
        {
            var upperLetters = Regex.Matches(value, @"[A-Z]")
                    .Cast<Match>().Select(m => m.Value);

            return String.Join("", upperLetters);
        }
    }
}
