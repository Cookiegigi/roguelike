﻿using System;
using System.Collections.Generic;
using System.Linq;

//code from : https://github.com/RafaelKuebler/DelaunayVoronoi
namespace DelaunayVoronoi
{
    public class DelaunayTriangulator
    {
        public int Seed { get; set; }
        public List<Point> Points { get; set; } = new List<Point>();
        public HashSet<Triangle> Triangles { get; set; } = new HashSet<Triangle>();

        public double MaxX { get; private set; }
        public double MaxY { get; private set; }
        private IEnumerable<Triangle> border;

        public DelaunayTriangulator(int nbPoints, double maxX, double maxY, int seed)
        {
            Seed = seed;

            GeneratePoints(nbPoints, maxX, maxY);
            BowyerWatson();
        }

        private void GeneratePoints(int amount, double maxX, double maxY)
        {
            MaxX = maxX;
            MaxY = maxY;

            // TODO make more beautiful
            var point0 = new Point(0, 0);
            var point1 = new Point(0, MaxY);
            var point2 = new Point(MaxX, MaxY);
            var point3 = new Point(MaxX, 0);
            Points = new List<Point>() { point0, point1, point2, point3 };
            var tri1 = new Triangle(point0, point1, point2);
            var tri2 = new Triangle(point0, point2, point3);
            border = new List<Triangle>() { tri1, tri2 };

            var random = new Random(Seed);
            for (int i = 0; i < amount - 4; i++)
            {
                var pointX = random.NextDouble() * MaxX;
                var pointY = random.NextDouble() * MaxY;
                Points.Add(new Point(pointX, pointY));
            }
        }

        private void BowyerWatson()
        {
            //var supraTriangle = GenerateSupraTriangle();
            Triangles = new HashSet<Triangle>(border);

            foreach (var point in Points)
            {
                var badTriangles = FindBadTriangles(point, Triangles);
                var polygon = FindHoleBoundaries(badTriangles);

                foreach (var triangle in badTriangles)
                {
                    foreach (var vertex in triangle.Vertices)
                    {
                        vertex.AdjacentTriangles.Remove(triangle);
                    }
                }
                Triangles.RemoveWhere(o => badTriangles.Contains(o));

                foreach (var edge in polygon)
                {
                    var triangle = new Triangle(point, edge.Point1, edge.Point2);
                    Triangles.Add(triangle);
                }
            }

            //triangulation.RemoveWhere(o => o.Vertices.Any(v => supraTriangle.Vertices.Contains(v)));
        }

        private List<Edge> FindHoleBoundaries(ISet<Triangle> badTriangles)
        {
            var edges = new List<Edge>();
            foreach (var triangle in badTriangles)
            {
                edges.Add(new Edge(triangle.Vertices[0], triangle.Vertices[1], true));
                edges.Add(new Edge(triangle.Vertices[1], triangle.Vertices[2], true));
                edges.Add(new Edge(triangle.Vertices[2], triangle.Vertices[0], true));
            }
            var grouped = edges.GroupBy(o => o);
            var boundaryEdges = edges.GroupBy(o => o).Where(o => o.Count() == 1).Select(o => o.First());
            return boundaryEdges.ToList();
        }

        private Triangle GenerateSupraTriangle()
        {
            //   1  -> maxX
            //  / \
            // 2---3
            // |
            // v maxY
            var margin = 500;
            var point1 = new Point(0.5 * MaxX, -2 * MaxX - margin);
            var point2 = new Point(-2 * MaxY - margin, 2 * MaxY + margin);
            var point3 = new Point(2 * MaxX + MaxY + margin, 2 * MaxY + margin);
            return new Triangle(point1, point2, point3);
        }

        private ISet<Triangle> FindBadTriangles(Point point, HashSet<Triangle> triangles)
        {
            var badTriangles = triangles.Where(o => o.IsPointInsideCircumcircle(point));
            return new HashSet<Triangle>(badTriangles);
        }
    }
}