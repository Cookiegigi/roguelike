﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


/// <summary>
/// New type create by myself and integrate in voronoi generation
/// </summary>

namespace DelaunayVoronoi
{
    public class Polygon
    {
        public Point Center { get; set; }
        public HashSet<Edge> Edges { get; set; } = new HashSet<Edge>();
        public HashSet<Polygon> Neighbour
        {
            get
            {
                HashSet<Polygon> result = new HashSet<Polygon>();

                foreach (Edge e in Edges)
                {
                    result.Add(e.Polygons.Where(p => p != this).First());
                }

                return result;
            }
        }
        public HashSet<Point> Vertices
        {
            get
            {
                HashSet<Point> result = new HashSet<Point>();

                foreach (Edge e in Edges)
                {
                    result.Add(e.Point1);
                    result.Add(e.Point2);
                }

                return result;
            }
        }

        public Polygon()
        {

        }

        /// <summary>
        /// Copy an existing polygon
        /// </summary>
        /// <param name="p"></param>
        public Polygon(Polygon p) : this()
        {
            Center = p.Center;


            foreach (Edge e in p.Edges)
            {
                e.Polygons.Remove(p);
                e.Polygons.Add(this);
            }

            Edges = new HashSet<Edge>(p.Edges);
        }

        //Debug value
        public bool IsComplete()
        {
            List<Edge> temp = new List<Edge>(Edges);

            Edge start = temp[0];
            Edge current, last;

            temp.Remove(start);

            last = start;
            current = start.Neighbour1;

            while (temp.Count != 0 && current != start && current != null)
            {
                temp.Remove(current);

                if (current.Neighbour1 == last)
                {
                    last = current;

                    current = last.Neighbour2;
                }
                else
                {
                    last = current;

                    current = last.Neighbour1;
                }
            }

            return temp.Count == 0 && current != null;
        }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            else if (!(obj is Polygon)) return false;
            else
            {
                Polygon p = (Polygon)obj;

                if (Edges.Count != p.Edges.Count)
                {
                    return false;
                }

                bool result = false;

                var temp = Edges.GetEnumerator();
                temp.MoveNext();

                while (p.Edges.Contains(temp.Current))
                {
                    temp.MoveNext();
                }

                if (temp.Current == null)
                {
                    result = true;
                }

                return result;
            }
        }

        /// <summary>
        /// Smooth polygon's edges with Bezier curve
        /// </summary>
        /// <param name="n"> number of subdivision for one edge</param>
        public void Smooth(int n)
        {
            //Calculate control point for each vertice
            List<Point> vertices = Vertices.ToList();
            List<List<Point>> controlPoint = GetControlsPoints(vertices);

            // smooth each edge
            //temporary list for new edge
            HashSet<Edge> newEdges = new HashSet<Edge>();

            Point start, stop, cstart, cstop;

            foreach (Edge e in Edges)
            {
                // fix start and stop point of e
                start = e.Point1;
                stop = e.Point2;

                // get control point associated with start and stop
                cstart = GetControlsPointAssociated(e, start, controlPoint[vertices.IndexOf(start)]);
                cstop = GetControlsPointAssociated(e, stop, controlPoint[vertices.IndexOf(stop)]);

                //clear ancient edge from start and stop
                start.AdjacentEdges.Remove(e);
                stop.AdjacentEdges.Remove(e);

                // calculate new edge with bezier curve
                // Bezier curve for 0 < t < 1
                Func<double, Point> bezier =
                    (t) => start * (1 - t) * (1 - t) * (1 - t)
                    + cstart * 3 * t * (1 - t) * (1 - t)
                    + cstop * 3 * t * t * (1 - t)
                    + stop * t * t * t;

                double step = 1.0 / n;
                Edge last, temp;
                Point p1 = start, p2;
                for (int i = 0; i < n; i++)
                {
                    //calculate next point
                    if (i + 1 == n)
                    {
                        p2 = stop;
                    }
                    else
                    {
                        p2 = bezier((i + 1) * step);
                    }

                    temp = new Edge(p1, p2);

                    // add polygon neighbour of the old to the new one
                    temp.Polygons = e.Polygons;

                    p1.AdjacentEdges.Add(temp);
                    p2.AdjacentEdges.Add(temp);


                    newEdges.Add(temp);

                    p1 = p2;
                }
            }

            // replace old edges with new edges
            Edges = newEdges;

        }

        /// <summary>
        /// Complicated function to get control point assosciated at one edge
        /// </summary>
        /// <param name="e"></param>
        /// <param name="p"></param>
        /// <param name="list"></param>
        /// <returns></returns>
        private Point GetControlsPointAssociated(Edge e, Point p, List<Point> list)
        {

            Edge temp, temp1, temp2;
            double a, a1, a2;

            // create temporary edge
            temp = new Edge(Center, p, noId: true, noNeighbour: true);
            temp1 = new Edge(p, list[0], noId: true, noNeighbour: true);
            temp2 = new Edge(p, list[1], noId: true, noNeighbour: true);

            //calculate angle
            a = Edge.CalculateAngleWithNeighbour(temp, e, p);
            a1 = Edge.CalculateAngleWithNeighbour(temp, temp1, p);
            a2 = Edge.CalculateAngleWithNeighbour(temp, temp2, p);

            //choose control point 
            if(a >= 0 && a1 >= 0 || a <= 0 && a1 <= 0)
            {
                return list[0];
            }
            else
            {
                return list[1];
            }

        }

        /// <summary>
        /// Method to calculate control point in a polygon for Bezier interpolation
        /// </summary>
        /// <remarks>
        /// source : http://www.antigrain.com/research/bezier_interpolation/
        /// </remarks>
        /// <param name="vertices"></param>
        /// <returns></returns>
        private List<List<Point>> GetControlsPoints(List<Point> vertices)
        {
            List<List<Point>> result = new List<List<Point>>();

            Edge e1, e2;

            List<Edge> temp;

            foreach (Point vert in vertices)
            {
                // get all neighbour edge of vert which are in this polygon
                temp = Edges.Where(e => e.Point1 == vert || e.Point2 == vert).ToList();

                Point p1, p2;
                p1 = (temp[0].Point1 != vert) ? temp[0].Point1 : temp[0].Point2;
                p2 = (temp[1].Point1 != vert) ? temp[1].Point1 : temp[1].Point2;

                // choose e1 and e2
                // e1 is on the left x from vert
                // e2 is on the right x from vert
                if (p1.X <= vert.X && p2.X <= vert.X && p1.Y < p2.Y ||
                    p1.X <= vert.X && p2.X > vert.X)
                {
                    e1 = temp[0];
                    e2 = temp[1];
                }
                else
                {
                    e1 = temp[1];
                    e2 = temp[0];
                }

                // calculate middle point of each edge
                Point a1 = e1.MiddlePoint;
                Point a2 = e2.MiddlePoint;

                // calculate length of each edge
                double l1 = e1.Norm;
                double l2 = e2.Norm;

                // calculate length between a1 and a2
                double l12 = Math.Sqrt((a1.X - a2.X) * (a1.X - a2.X) +
                    (a1.Y - a2.Y) * (a1.Y - a2.Y));

                // calculate distance with a point p on (a1, a2) 
                // ||a1, p|| = d12, ||a2, p|| = d21
                // L1/L2 = d12/d21
                double d21 = l12 / (l1 / l2 + 1);
                double d12 = l12 - d21;

                // calculate director vector of (a1, a2)
                Point c = new Point
                    (
                        (a2.X - a1.X) / l12,
                        (a2.Y - a1.Y) / l12
                    );

                //calculate control point 
                Point c1 = vert - d12 * c;
                Point c2 = vert + d21 * c;

                result.Add(new List<Point>() { c1, c2 });
            }

            return result;
        }
    }
}
