﻿//code from : https://github.com/RafaelKuebler/DelaunayVoronoi
// code custom by me


using System;
using System.Collections.Generic;
using System.Linq;

namespace DelaunayVoronoi
{
    public class Edge
    {
        //Debug Id
        public static int IdCount { get; private set; } = 0;
        public int Id { get; private set; }
        //

        public Point Point1 { get; }
        public Point Point2 { get; }

        public HashSet<Polygon> Polygons { get; set; } = new HashSet<Polygon>();

        public Edge Neighbour1
        {
            get
            {
                return Point1.AdjacentEdges.Where(e => e != this).FirstOrDefault();
            }
        }

        public Edge Neighbour2
        {
            get
            {
                return Point2.AdjacentEdges.Where(e => e != this).FirstOrDefault();
            }
        }

        public Point MiddlePoint
        {
            get
            {
                return new Point
                (
                    (Point1.X + Point2.X)/2,
                    (Point1.Y + Point2.Y)/2
                );
            }
        }

        public double Norm
        {
            get
            {
                return Math.Sqrt((Point1.X - Point2.X) * (Point1.X - Point2.X) 
                    + (Point1.Y - Point2.Y) * (Point1.Y - Point2.Y));
            }

        }

        // Coeff1 * x + Coeff2 * y + Coeff3 = 0 
        public double Coeff1 { get; private set; }
        public double Coeff2 { get; private set; }
        public double Coeff3 { get; private set; }

        public Edge(Point point1, Point point2, bool noId = false, bool noNeighbour = false)
        {
            Point1 = point1;
            Point2 = point2;


            if (!noNeighbour)
            {
                Point1.AdjacentEdges.Add(this);
                Point2.AdjacentEdges.Add(this);
            }
            
            UpdateCoeff();

            if (!noId)
            {
                Id = IdCount++;
            }
            
        }

        /// <summary>
        /// Update coefficient of line equation
        /// </summary>
        private void UpdateCoeff()
        {
            double x1, y1, x2, y2;

            if (Point1.X - Point2.X <= 0)
            {
                x1 = Point1.X;
                y1 = Point1.Y;
                x2 = Point2.X;
                y2 = Point2.Y;
            }
            else
            {
                x2 = Point1.X;
                y2 = Point1.Y;
                x1 = Point2.X;
                y1 = Point2.Y;
            }
            
            
            Coeff1 = (y2 - y1);

            Coeff2 = -(x2 - x1);

            Coeff3 = -(Coeff1 * x1 + Coeff2 * y1);
        }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            if (obj.GetType() != GetType()) return false;
            var edge = obj as Edge;

            var samePoints = Point1 == edge.Point1 && Point2 == edge.Point2;
            var samePointsReversed = Point1 == edge.Point2 && Point2 == edge.Point1;
            return samePoints || samePointsReversed;
        }

        public override int GetHashCode()
        {
            int hCode = (int)Point1.X ^ (int)Point1.Y ^ (int)Point2.X ^ (int)Point2.Y;
            return hCode.GetHashCode();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"> y when the line is vertical </param>
        /// <returns></returns>
        public double YOnEdge(double x, double y)
        {
            if(double.IsNaN(Coeff1 / (-Coeff2)))
            {
                return y;
            }
            else
            {
                return Coeff1 / (-Coeff2) * x + Coeff3 / (-Coeff2);
            }
        }

        public double YOnEdge(double x)
        {
            return YOnEdge(x, Point1.Y);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="y"></param>
        /// <param name="x">x when the line is horizontal</param>
        /// <returns></returns>
        public double XOnEdge(double y, double x)
        {
            if (double.IsNaN(Coeff2 / Coeff1))
            {
                return x;
            }
            else
            {
                return Coeff2 / (-Coeff1) * y + Coeff3 / (-Coeff1);
            }
        }

        public double XOnEdge(double y)
        {
            return XOnEdge(y, Point1.X);
        }

        

        // scalar product
        public double Scal(Edge e, bool normalize = false)
        {
            double result = 0;

            double[] v1 = { Point2.X - Point1.X, Point2.Y - Point1.Y };
            double[] v2 = { e.Point2.X - e.Point1.X, e.Point2.Y - e.Point1.Y };

            result = v1[0] * v2[0] + v1[1] * v2[1];

            if (normalize)
            {
                result /= Math.Sqrt(v1[0] * v1[0] + v1[1] * v1[1]);
                result /= Math.Sqrt(v2[0] * v2[0] + v2[1] * v2[1]);
            }
            

            return result;
        }

        // determinant
        public double Det(Edge e, bool normalize = false)
        {
            double result = 0;

            double[] v1 = { Point2.X - Point1.X, Point2.Y - Point1.Y };
            double[] v2 = { e.Point2.X - e.Point1.X, e.Point2.Y - e.Point1.Y };

            result = v1[0] * v2[1] - v1[1] * v2[0];

            if (normalize)
            {
                result /= Math.Sqrt(v1[0] * v1[0] + v1[1] * v1[1]);
                result /= Math.Sqrt(v2[0] * v2[0] + v2[1] * v2[1]);
            }

            return result;
        }

        
        /// <summary>
        /// Reverse points
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        public static Edge Reverse(Edge e)
        {
            return new Edge(e.Point2, e.Point1, true, true);
        }

        public static double CalculateAngleWithNeighbour(Edge e1, Edge e2, Point edgePoint)
        {
            Edge temp1, temp2;

            double scal, det, result;

            if (edgePoint == e1.Point2 && edgePoint == e2.Point2)
            {
                scal = e1.Scal(e2, true);
                det = e1.Det(e2, true);

                result = ((det < 0) ? -1 : 1) * Math.Acos(scal);
            }
            else if (edgePoint == e1.Point1 && edgePoint == e2.Point2)
            {
                temp1 = Edge.Reverse(e1);

                scal = temp1.Scal(e2, true);
                det = temp1.Det(e2, true);

                result = ((det < 0) ? -1 : 1) * Math.Acos(scal);

            }
            else if (edgePoint == e1.Point1 && edgePoint == e2.Point1)
            {
                temp1 = Edge.Reverse(e1);
                temp2 = Edge.Reverse(e2);

                scal = temp1.Scal(temp2, true);
                det = temp1.Det(temp2, true);

                result = ((det < 0) ? -1 : 1) * Math.Acos(scal);
            }
            else
            {
                temp2 = Edge.Reverse(e2);

                scal = e1.Scal(temp2, true);
                det = e1.Det(temp2, true);

                result = ((det < 0) ? -1 : 1) * Math.Acos(scal);

            }

            return result;
        }


    }
}