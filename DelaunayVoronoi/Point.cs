﻿using System;
using System.Collections.Generic;

//code from : https://github.com/RafaelKuebler/DelaunayVoronoi


namespace DelaunayVoronoi
{
    public class Point
    {

        public double X { get; set; }
        public double Y { get; set; }
        public HashSet<Triangle> AdjacentTriangles { get; } = new HashSet<Triangle>();
        public HashSet<Edge> AdjacentEdges { get; set; } = new HashSet<Edge>();

        public Point(double x, double y)
        {
            X = x;
            Y = y;
            
        }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            if (!(obj is Point)) return false;

            Point temp = (Point)obj;
            return X == temp.X && Y == temp.Y;
        }

        public override int GetHashCode()
        {
            return (int)X ^ (int)Y;
        }

        /// <summary>
        /// Copy an existing point
        /// </summary>
        /// <param name="point"></param>
        public Point(Point point) : this(point.X, point.Y)
        {
            AdjacentEdges = point.AdjacentEdges;
            AdjacentTriangles = point.AdjacentTriangles;
        }


        /// <summary>
        /// Convert to an array of length 2
        /// </summary>
        /// <returns></returns>
        public int[] ToIntegerArray()
        {
            return new int[] { (int)X, (int)Y };
        }


        public static double Distance(Point p1, Point p2)
        {
            return Math.Sqrt((p1.X - p2.X) * (p1.X - p2.X) + (p1.Y - p2.Y) * (p1.Y - p2.Y));
        }

        public static Point operator * (Point p, double l)
        {
            return new Point
                (
                    p.X * l,
                    p.Y * l
                );
        }

        public static Point operator *(double l, Point p)
        {
            return p * l;
        }

        public static Point operator +(Point p1, Point p2)
        {
            return new Point
                (
                    p1.X + p2.X,
                    p1.Y + p2.Y
                );
        }

        public static Point operator -(Point p1, Point p2)
        {
            return p1 + p2 * (-1);
        }
    }
}