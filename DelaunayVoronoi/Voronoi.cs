﻿using System;
using System.Collections.Generic;
using System.Linq;

//code from : https://github.com/RafaelKuebler/DelaunayVoronoi


namespace DelaunayVoronoi
{
    public class Voronoi
    {
        public HashSet<Edge> Edges { get; set; } = new HashSet<Edge>();
        public HashSet<Point> Points { get; set; } = new HashSet<Point>();
        public HashSet<Polygon> Polygons { get; set; } = new HashSet<Polygon>();

        public Voronoi(DelaunayTriangulator delaunay)
        {
            GenerateVoronoiFromDelaunay(delaunay.Triangles, delaunay.MaxX, delaunay.MaxY);
            ExtractPolygons(delaunay.Points);
        }

        private void GenerateVoronoiFromDelaunay(IEnumerable<Triangle> triangulation
            , double maxX, double maxY)
        {
            Point p1, p2;
            Edge edge;

            foreach (var triangle in triangulation)
            {
                foreach (var neighbor in triangle.TrianglesWithSharedEdge)
                {
                    p1 = triangle.Circumcenter;
                    p2 = neighbor.Circumcenter;

                    AddEdge(p1, p2);
                }
            }

            //Add corner point
            Point p3, p4;

            AddPointToPoints(-10, -10, out p1);
            AddPointToPoints(-10, maxY + 10, out p2);
            AddPointToPoints(maxX + 10, -10, out p3);
            AddPointToPoints(maxX +10, maxY +10, out p4);


            //Add limit edge
            Point pminX, pminY, pmaxX, pmaxY;

            var temp = Points.Where(p => p != p1 && p != p2 && p != p3 && p != p4);
            var tempX = temp.Select(p => p.X);
            var tempY = temp.Select(p => p.Y);

            pminX = Points.Where(p => p.X == tempX.Min()).First();
            pminY = Points.Where(p => p.Y == tempY.Min()).First();
            pmaxX = Points.Where(p => p.X == tempX.Max()).First();
            pmaxY = Points.Where(p => p.Y == tempY.Max()).First();

            AddEdge(p1, pminX);
            AddEdge(p2, pminX);
            AddEdge(p1, pminY);
            AddEdge(p3, pminY);
            AddEdge(p3, pmaxX);
            AddEdge(p4, pmaxX);
            AddEdge(p4, pmaxY);
            AddEdge(p2, pmaxY);

        }

        private void AddEdge(Point p1, Point p2)
        {
            Edge edge = new Edge(p1, p2);

            p1.AdjacentEdges.Add(edge);
            p2.AdjacentEdges.Add(edge);

            Edges.Add(edge);
            Points.Add(p1);
            Points.Add(p2);
        }

        private Point FindNearestCorner(Point p, Point[] corner)
        {
            double currentDist = -1, temp;
            Point current = null;

            foreach (Point c in corner)
            {
                temp = Math.Sqrt((p.X - c.X) * (p.X - c.X) + (p.Y - c.Y) * (p.Y - c.Y));

                if (currentDist == -1 || currentDist > temp)
                {
                    currentDist = temp;
                    current = c;
                }
            }

            return current;
        }

        /// <summary>
        /// Verify if Points contains a point with coordinate <paramref name="x"/> and <paramref name="y"/>
        /// if true : return the existing point
        /// else create new one and add it to pOints
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="point"></param>
        private void AddPointToPoints(double x, double y, out Point point)
        {
            point = new Point(x, y);

            Point temp = new Point(point);
            if (Points.Contains(point))
            {
                point = Points.Where(p => p == temp).First();
            }
            else
            {
                Points.Add(point);
            }
        }

        /// <summary>
        /// Populate the list of polygon
        /// </summary>
        /// <param name="points"></param>
        public void ExtractPolygons(IEnumerable<Point> points)
        {
            foreach (Point p in points)
            {
                Polygons.Add(ExtractPolygon(p));
            }
        }

        /// <summary>
        /// Get the polygon associated at <paramref name="point"/>
        /// </summary>
        /// <param name="point"></param>
        /// <returns></returns>
        private Polygon ExtractPolygon(Point point)
        {
            Polygon result = new Polygon();

            // init "center" of polygon
            result.Center = point;

            //find one edge of this polygon
            Edge start = FindOneEdge(point);

            // add this edge in polygon
            result.Edges.Add(start);
            start.Polygons.Add(result);

            Edge last, current;
            Point aimedPoint;

            last = start;
            aimedPoint = start.Point1;

            current = ChooseNeighbour(start, aimedPoint, point);


            while (current != start)
            {
                result.Edges.Add(current);
                current.Polygons.Add(result);

                if (current.Point1 == aimedPoint)
                {
                    last = current;
                    aimedPoint = current.Point2;

                    current = ChooseNeighbour(current, aimedPoint, point);
                }
                else
                {
                    last = current;
                    aimedPoint = current.Point1;

                    current = ChooseNeighbour(current, aimedPoint, point);
                }
            }

            return result;
        }

        /// <summary>
        /// Choose the neighbourn of <paramref name="current"/> which is in the same polygon
        /// </summary>
        /// <param name="current"></param>
        /// <param name="edgePoint"></param>
        /// <param name="point"></param>
        /// <returns></returns>
        private Edge ChooseNeighbour(Edge current, Point edgePoint, Point point)
        {
            Edge temp;

            temp = new Edge(point, edgePoint, true, true);

            List<Edge> potentialNeighbour = edgePoint.AdjacentEdges
                .Where(e => !(e.Equals(current))).ToList();
            if (potentialNeighbour.Count == 1)
            {
                return potentialNeighbour[0];
            }

            List<double> angles = CalculateAngleWithNeighbours(temp, potentialNeighbour, edgePoint);

            //edge position / central point
            double angle = Edge.CalculateAngleWithNeighbour(temp, current, edgePoint);

            // sens anti-trigo
            if (angle < 0)
            {
                return potentialNeighbour[angles.IndexOf(angles.Where(a => a >= 0).Min())];
            }
            else
            {
                return potentialNeighbour[angles.IndexOf(angles.Where(a => a < 0).Max())];
            }

        }

        private List<double> CalculateAngleWithNeighbours(Edge edge, List<Edge> edges, Point edgePoint)
        {
            List<double> result = new List<double>();

            foreach (Edge e in edges)
            {
                result.Add(Edge.CalculateAngleWithNeighbour(edge, e, edgePoint));

            }

            return result;
        }

        /// <summary>
        /// Find one edge of the polygon with the center <paramref name="point"/>
        /// </summary>
        /// <param name="point"></param>
        /// <returns></returns>
        private Edge FindOneEdge(Point point)
        {
            
            List<Edge> edges = IntersectWithHorizontal((int)point.Y);
            List<double> distance = DistancePointEdges(edges, point);

            // the edge is the edge with the nearest intersection point
            return edges[distance.IndexOf(distance.Min())];
        }

        /// <summary>
        /// Search all edge which have an intersection with the horizontal line (h = <paramref name="y"/>)
        /// </summary>
        /// <param name="y"></param>
        /// <returns></returns>
        private List<Edge> IntersectWithHorizontal(int y)
        {
            return Edges.Where((e) =>
            {
                int min, max;
                min = Math.Min((int)e.Point1.Y, (int)e.Point2.Y);
                max = Math.Max((int)e.Point1.Y, (int)e.Point2.Y);

                return y >= min && y <= max;
            }).ToList();
        }

        /// <summary>
        /// Calculate the distance between <paramref name="point"/> and <paramref name="edges"/>
        /// </summary>
        /// <param name="edges"></param>
        /// <param name="point"></param>
        /// <returns></returns>
        private List<double> DistancePointEdges(List<Edge> edges, Point point)
        {
            List<double> result = new List<double>();

            foreach (Edge e in edges)
            {
                result.Add(DistancePointEdge(e, point));
            }

            return result;
        }

        private double DistancePointEdge(Edge e, Point point)
        {
            var temp = e.XOnEdge(point.Y, point.X);

            double result = Math.Abs(temp - point.X);
            return result;
        }
    }
}