﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility.DataType;
using Microsoft.Xna.Framework.Input;

namespace Game.Data.Global
{
    /// <summary>
    /// Contains the keys map which is used by the player
    /// </summary>
    public static class KeysMap
    {
        public static GameData<Keys> Map { get; private set; }

        public static void Init(string path)
        {
            Map = new GameData<Keys>(path);
            KeysMapInit();
        }

        private static void KeysMapInit()
        {
            string[] labels = { "Up", "Down", "Left", "Right" 
                    , "+", "-", "North", "East", "South", "West", "Open Map", "Close Map",
            "Switch Mode"};
            Keys[] defaultKeys = { Keys.Z, Keys.S, Keys.Q, Keys.D
                    , Keys.Add, Keys.Subtract , Keys.Up, Keys.Right
                    , Keys.Down, Keys.Left, Keys.M, Keys.Escape
            , Keys.Tab};

            for(int i = 0; i < labels.Length; i++)
            {
                if (!Map.Labels.Contains(labels[i]))
                {
                    Map[labels[i]] = defaultKeys[i];
                }
            }

            Map.Save();
        }
    }
}
