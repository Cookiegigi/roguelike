﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Game.DataType.WorldType;
using Game.Generators;

namespace Game.Data.Global
{
    /// <summary>
    /// Contains the world where the player is
    /// </summary>
    public static class GlobalWorld
    {
        private static World world;
        
        public static World CurrentWorld
        {
          
            get
            {
                //lazy loading
                if(world == null)
                {
                    world = new World(100, TrueRandomGenerator.GetRandomNumber());
                }
                return world;
            }
            set
            {
                world = value;
            }
        }

        
    }
}
