﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Game.DataType.ZoneType.Biomes;

namespace Game.Data.Zone
{
    /// <summary>
    /// Contains some enumeration for biomes
    /// </summary>
    public static class Biomes
    {
        //public static Type[] BiomesList { get; set; }

        //static Biomes()
        //{
        //    BiomesList = new Type[] 
        //    {

        //    };

        //}

        //public static Type[] GetBiomeDependOn(HeatType heat, MoistureType moisture)
        //{
        //    var result = BiomesList.Where((b) =>
        //    {
        //        var heat2 = b.GetField("HeatType").GetValue(null);
        //        var moisture2 = b.GetField("MoistureType").GetValue(null);
        //        //var height2 = b.GetProperty("HeightType", System.Reflection.BindingFlags.Static).GetValue(null, null);

        //        return (heat2 != null && ((HeatType[])heat2).Contains(heat))
        //                && (moisture2 != null && ((MoistureType[])moisture2).Contains(moisture))
        //                ;
        //    }
        //    );

        //    return result.ToArray();
        //}

        public static int HeightTypeCount
        {
            get
            {
                return Enum.GetNames(typeof(HeightType)).Length;
            }
        }
        public static int[] HeightTypeValues
        {
            get
            {
                List<int> result = new List<int>();

                foreach (int v in Enum.GetValues(typeof(HeightType)))
                {
                    result.Add(v);
                }

                return result.ToArray();
            }
        }
        public enum HeightType
        {
            SeaLevel,
            BeachLevel,
            PlainLevel,
            LowMountainLevel,
            MountainLevel,
            SubAlpinLevel,
            AlpinLevel,
            LapiesLevel
        }

        public static int HeatTypeCount
        {
            get
            {
                return Enum.GetNames(typeof(HeatType)).Length;
            }
        }
        public static int[] HeatTypeValues
        {
            get
            {
                List<int> result = new List<int>();

                foreach (int v in Enum.GetValues(typeof(HeatType)))
                {
                    result.Add(v);
                }

                return result.ToArray();
            }
        }
        public enum HeatType
        {
            TropicalHeat,
            TemperateWarmHeat,
            TemperateColdHeat,
            BorealHeat,
            SubPolarHeat,
            PolarHeat
        }

        public static int MoistureTypeCount
        {
            get
            {
                return Enum.GetNames(typeof(MoistureType)).Length;
            }
        }
        public static int[] MoistureTypeValues
        {
            get
            {
                List<int> result = new List<int>();

                foreach (int v in Enum.GetValues(typeof(MoistureType)))
                {
                    result.Add(v);
                }

                return result.ToArray();
            }
        }
        public enum MoistureType
        {
            OverDry,
            VeryDry,
            Dry,
            MediumDry,
            LittleDry,
            Wet,
            VeryWet,
            OverWet

        }
    }
}
