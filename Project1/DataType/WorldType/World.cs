﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Game.DataType.Global;
using Game.DataType.ZoneType;
using Game.DataType.ZoneType.Biomes;
using Game.Generators.WorldGenerators;
using Game.Generators.ZoneGenerators;

namespace Game.DataType.WorldType
{
    /// <summary>
    /// Collections for zones
    /// </summary>

    public class World
    {
        public List<Zone> Zones { get; private set; }

        public Zone CurrentZone { get; set; }

        private WorldGenerator generator;

        public int Seed { get; private set; } = 0;


        public World(int seed = 0)
        {
            Seed = seed;

            Zones = new List<Zone>();

            generator = new WorldGenerator(this);

            CurrentZone = generator.GenerateNewZone(new Adress(new int[] { 0, 0 }));
            Zones.Add(CurrentZone);
        }

        public World(int numberOfPregenerateCase, int seed) : this(seed)
        {
            generator.Pregenerate(numberOfPregenerateCase);
        }

        public void Move(CardinalPoint direction)
        {
            if (CurrentZone.AdjacentZones[(int)direction] == null)
            {
                GenerateNewZone(direction);
            }
            CurrentZone = CurrentZone.AdjacentZones[(int)direction];
            CurrentZone.GatesState[((int)direction + 2) % 4] = true;
        }

        public void GenerateNewZone(Adress adress)
        {
            Zone newZone = generator.GenerateNewZone(adress);
            InsertZone(newZone);
        }

        public void GenerateNewZone(CardinalPoint direction)
        {
            GenerateNewZone(GetAdressInDirection(direction));
        }

        public void InsertZone(Zone zone)
        {
            Zones.Add(zone);

            //South
            Zone sZone = GetZoneByAdress(GetAdressInDirection(zone.ZoneAdress, CardinalPoint.South));
            if (sZone != null)
            {
                zone.South = sZone;
                sZone.North = zone;
            }

            //North
            Zone nZone = GetZoneByAdress(GetAdressInDirection(zone.ZoneAdress, CardinalPoint.North));
            if (nZone != null)
            {
                zone.North = nZone;
                nZone.South = zone;
            }

            //East
            Zone eZone = GetZoneByAdress(GetAdressInDirection(zone.ZoneAdress, CardinalPoint.East));
            if (eZone != null)
            {
                zone.East = eZone;
                eZone.West = zone;
            }

            //West
            Zone wZone = GetZoneByAdress(GetAdressInDirection(zone.ZoneAdress, CardinalPoint.West));
            if (wZone != null)
            {
                zone.West = wZone;
                wZone.East = zone;
            }
        }

        public Zone[] GetAdjacentZoneOfAnAdress(Adress adress)
        {
            Zone[] result = new Zone[4];

            result[(int)CardinalPoint.North] = GetZoneByAdress(
                GetAdressInDirection(adress, CardinalPoint.North)
                );
            result[(int)CardinalPoint.South] = GetZoneByAdress(
                GetAdressInDirection(adress, CardinalPoint.South)
                );
            result[(int)CardinalPoint.West] = GetZoneByAdress(
                GetAdressInDirection(adress, CardinalPoint.West)
                );
            result[(int)CardinalPoint.East] = GetZoneByAdress(
                GetAdressInDirection(adress, CardinalPoint.East)
                );

            return result;
        }

        public Biome[] GetAdjacentZoneBiomeOfAnAdress(Adress adress)
        {
            Biome[] result = new Biome[4];

            int c = 0;
            foreach(Zone z in GetAdjacentZoneOfAnAdress(adress))
            {
                if(z != null)
                {
                    result[c] = z.ZoneBiome;
                }
                c++;
            }

            return result;
        }

        public Zone GetZoneByAdress(int x, int y)
        {
            return GetZoneByAdress(new Adress{ X=x, Y=y });
        }

        public Adress GetAdressInDirection(Adress adress, CardinalPoint direction)
        {
            Adress result = new Adress() { X = int.MaxValue, Y = int.MaxValue };

            if (direction == CardinalPoint.North)
            {
                result = new Adress
                {
                    X = adress.X
                    ,
                    Y = adress.Y + 1
                };
            }
            else if (direction == CardinalPoint.South)
            {
                result = new Adress
                {
                    X = adress.X
                    ,
                    Y = adress.Y - 1
                };
            }
            else if (direction == CardinalPoint.East)
            {
                result = new Adress
                {
                    X = adress.X + 1
                    ,
                    Y = adress.Y
                };
            }
            else if (direction == CardinalPoint.West)
            {
                result = new Adress
                {
                    X = adress.X - 1
                    ,
                    Y = adress.Y
                };
            }
            return result;
        }

        public Adress GetAdressInDirection(CardinalPoint direction)
        {
            return GetAdressInDirection(CurrentZone.ZoneAdress, direction);
        }

        public Zone GetZoneByAdress(Adress adress)
        {
            int c = 0;

            while(
                (c < Zones.Count && (Zones[c].ZoneAdress.X != adress.X
                || Zones[c].ZoneAdress.Y != adress.Y)))
            {
                c++;
            }

            if(c == Zones.Count)
            {
                return null;
            }
            else
            {
                return Zones[c];
            }
        }
    }
}
