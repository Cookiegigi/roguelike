﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Game.Generators.ZoneGenerators;
using Microsoft.Xna.Framework;
using DelaunayVoronoi;

namespace Game.DataType.ZoneType.Layers
{
    public class PathLayer : Layer
    {
        public PathLayer(ZoneGenerator zoneGenerator, Zone zone) : base(zoneGenerator, zone)
        {
        }

        private Color[,] colors;

        public bool NorthPath { get; set; }
        public bool SouthPath { get; set; }
        public bool EastPath { get; set; }
        public bool WestPath { get; set; }

        public List<DelaunayVoronoi.Point> PathPoint;

        public override Color[,] Colors
        {
            get
            {
                // lazy loading
                if (colors == null)
                {
                    colors = zoneGenerator.GetZonePath(zone);
                }
                return colors;
            }
        }
    }
}
