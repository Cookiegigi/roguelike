﻿using Game.Generators.ZoneGenerators;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.DataType.ZoneType.Layers
{
    /// <summary>
    /// Layer of zone 
    /// Contain the texture of the layer
    /// </summary>
    public abstract class Layer
    {
        protected ZoneGenerator zoneGenerator;
        protected Zone zone;

        protected Layer(ZoneGenerator zoneGenerator, Zone zone)
        {
            this.zoneGenerator = zoneGenerator;
            this.zone = zone;
        }

        public abstract Color[,] Colors
        {
            get;
        }

    }
}
