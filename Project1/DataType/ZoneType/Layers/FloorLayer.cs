﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Game.Generators.ZoneGenerators;
using Microsoft.Xna.Framework;

namespace Game.DataType.ZoneType.Layers
{
    /// <summary>
    /// Layer specific for the floor of zone
    /// </summary>
    public class FloorLayer : Layer
    {
        public FloorLayer(ZoneGenerator zoneGenerator, Zone zone) : base(zoneGenerator, zone)
        {
        }

        private Color[,] colors;

        public override Color[,] Colors
        {
            get
            {
                //lazy loading
                if(colors == null)
                {
                    colors =  zoneGenerator.GetZoneFloor(zone);
                }
                return colors;
            }
        }

    }
}
