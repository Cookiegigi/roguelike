﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Game.Data.Zone;
using Utility.ToolBox;

namespace Game.DataType.ZoneType.Biomes
{
    /// <summary>
    /// Climate info about biome 'heat, moisture)
    /// </summary>
    public struct ClimateInfo
    {
        private Data.Zone.Biomes.HeatType temperature;
        private Data.Zone.Biomes.MoistureType moisture;

        public Data.Zone.Biomes.HeatType Temperature
        {
            get
            {
                return temperature;
            }
        }
        public string ContractTemperature
        {
            get
            {
                return StringToolBox.GetOnlyUpperLetters(temperature.ToString());
            }
        }


        public Data.Zone.Biomes.MoistureType Moisture
        {
            get
            {
                return moisture;
            }
        }
        public string ContractMoisture
        {
            get
            {
                return StringToolBox.GetOnlyUpperLetters(moisture.ToString());
            }
        }

        public ClimateInfo(Data.Zone.Biomes.HeatType temperature
            , Data.Zone.Biomes.MoistureType moisture)
        {
            this.temperature = temperature;
            this.moisture = moisture;
        }
    }
}
