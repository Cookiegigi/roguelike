﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Game.Data.Zone;

namespace Game.DataType.ZoneType.Biomes
{
    /// <summary>
    /// 
    /// </summary>
    public class Biome
    {
        public BiomeInfo General { get; private set; }

        public ClimateInfo Climate { get; private set; }

        public Biome(BiomeInfo general, ClimateInfo info)
        {
            General = general;
            Climate = info;
        }
    }
}
