﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utility.ToolBox;

namespace Game.DataType.ZoneType.Biomes
{
    /// <summary>
    /// General info about biome
    /// </summary>
    public struct BiomeInfo
    {
        private string name;
        private Color[] colorList;

        public BiomeInfo(string name, Color[] colorList)
        {
            this.name = name;
            this.colorList = colorList;
        }

        public string Name
        {
            get
            {
                return name;
            }
        }
        public Color[] ColorList
        {
            get
            {
                return colorList;
            }
        }
        public Color MainColor
        {
            get
            {
                return colorList[0];
            }
        }
        public string ContractName
        {
            get
            {
                return StringToolBox.GetOnlyUpperLetters(name);
            }
        }

    }
}
