﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.DataType.ZoneType
{
    /// <summary>
    /// basic structure for zone adress
    /// </summary>
    public struct Adress
    {
        public int X { get; set; }
        public int Y { get; set; }

        public Adress(int[] adress) : this()
        {
            X = adress[0];
            Y = adress[1];
        }

        public override string ToString()
        {
            return $"{X}, {Y}";
        }
    }
}
