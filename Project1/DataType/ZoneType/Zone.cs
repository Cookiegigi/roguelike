﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Game.DataType.Global;
using Game.DataType.ZoneType.Biomes;
using Game.Generators.ZoneGenerators;
using Microsoft.Xna.Framework;
using Game.DataType.ZoneType.Layers;
using DelaunayVoronoi;

namespace Game.DataType.ZoneType
{
    /// <summary>
    /// 
    /// </summary>
    public class Zone
    {
        /// <summary>
        /// Layer associated with this zone
        /// </summary>
        public List<Layer> Layers { get; private set; }

        /// <summary>
        /// Voronoi graph of this zone
        /// </summary>
        public Voronoi GraphVoronoi { get; set; }

        /// <summary>
        /// Delaunay graph of this zone
        /// </summary>
        public DelaunayTriangulator GraphDelaunay { get; set; }

        public FloorLayer Floor
        {
            get
            {
                FloorLayer result = null;

                result = Layers.Find((l) => l is FloorLayer) as FloorLayer;

                return result;
            }
        }

        public PathLayer Path
        {
            get
            {
                PathLayer result = null;

                result = Layers.Find((l) => l is PathLayer) as PathLayer;

                return result;
            }
        }

        public Adress ZoneAdress { get; set; }

        public Zone[] AdjacentZones { get; private set; }

        public Zone North
        {
            get
            {
                return AdjacentZones[(int)CardinalPoint.North];
            }

            set
            {
                AdjacentZones[(int)CardinalPoint.North] = value;
            }
        }

        public Zone East
        {
            get
            {
                return AdjacentZones[(int)CardinalPoint.East];
            }

            set
            {
                AdjacentZones[(int)CardinalPoint.East] = value;
            }
        }

        public Zone South
        {
            get
            {
                return AdjacentZones[(int)CardinalPoint.South];
            }

            set
            {
                AdjacentZones[(int)CardinalPoint.South] = value;
            }
        }

        public Zone West
        {
            get
            {
                return AdjacentZones[(int)CardinalPoint.West];
            }

            set
            {
                AdjacentZones[(int)CardinalPoint.West] = value;
            }
        }

        public bool[] GatesState { get; private set; }

        public bool NorthGate
        {
            get
            {
                return GatesState[(int)CardinalPoint.North];
            }

            set
            {
                GatesState[(int)CardinalPoint.North] = value;
            }
        }

        public bool SouthGate
        {
            get
            {
                return GatesState[(int)CardinalPoint.South];
            }

            set
            {
                GatesState[(int)CardinalPoint.South] = value;
            }
        }

        public bool EastGate
        {
            get
            {
                return GatesState[(int)CardinalPoint.East];
            }

            set
            {
                GatesState[(int)CardinalPoint.East] = value;
            }
        }

        public bool WestGate
        {
            get
            {
                return GatesState[(int)CardinalPoint.West];
            }

            set
            {
                GatesState[(int)CardinalPoint.West] = value;
            }
        }

        public Biome ZoneBiome { get; set; }

        public Zone(int x, int y, Biome biome) : this(new Adress { X=x, Y=y}, biome)
        {
            
        }

        public Zone(Adress adress, Biome biome)
        {
            ZoneAdress = adress;

            AdjacentZones = new Zone[4];
            GatesState = new bool[4] { true, true, true, true};

            ZoneBiome = biome;

            Layers = new List<Layer>();
        }
    }
}
