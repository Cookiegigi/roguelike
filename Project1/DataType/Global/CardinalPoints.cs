﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.DataType.Global
{

    public enum CardinalPoint
    {
        North,
        East,
        South,
        West
    }
}
