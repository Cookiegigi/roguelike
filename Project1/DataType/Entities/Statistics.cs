﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility.DataType;

namespace Game.DataType.Entities
{
    /// <summary>
    /// Class which handle the stat of the player
    /// </summary>
    public class Statistics : GameData<float>
    {
        public Statistics(string path) : base(path)
        {

        }
    }
}
