﻿using biome = Game.DataType.ZoneType.Biomes;
using heat = Game.Data.Zone.Biomes.HeatType;
using moisture = Game.Data.Zone.Biomes.MoistureType;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace Game.Generators.WorldGenerators
{
    /// <summary>
    /// 
    /// </summary>
    public static class BiomeFactory
    {
        private static Dictionary<biome.ClimateInfo, biome.BiomeInfo> biomeData; 

        /// <summary>
        /// Init biome data
        /// </summary>
        static BiomeFactory()
        {
            biomeData = new Dictionary<biome.ClimateInfo, biome.BiomeInfo>();

            biome.BiomeInfo info;

            #region Desert
            info = new biome.BiomeInfo("Desert"
                , new Color[] {
                    new Color(252, 255, 128),
                    new Color(255, 244, 128)
                });

            biomeData[new biome.ClimateInfo(heat.TropicalHeat, moisture.OverDry)]
                 = info;
            biomeData[new biome.ClimateInfo(heat.TemperateWarmHeat, moisture.OverDry)]
                 = info;
            biomeData[new biome.ClimateInfo(heat.TemperateColdHeat, moisture.OverDry)]
                 = info;
            biomeData[new biome.ClimateInfo(heat.BorealHeat, moisture.OverDry)]
                 = info;

            #endregion

            #region DryTundra

            info = new biome.BiomeInfo("DryTundra"
                , new Color[] {
                    new Color(202, 205, 139),
                    new Color(175, 205, 139)
                });

            biomeData[new biome.ClimateInfo(heat.SubPolarHeat, moisture.OverDry)]
                 = info;

            #endregion 

            #region PolarDesert

            info = new biome.BiomeInfo("PolarDesert"
                , new Color[] {
                    new Color(242, 240, 219),
                    new Color(218, 241, 240)
                });

            biomeData[new biome.ClimateInfo(heat.PolarHeat, moisture.OverDry)]
                 = info;
            biomeData[new biome.ClimateInfo(heat.PolarHeat, moisture.VeryDry)]
                 = info;
            biomeData[new biome.ClimateInfo(heat.PolarHeat, moisture.Dry)]
                 = info;

            #endregion

            #region Tundra

            info = new biome.BiomeInfo("Tundra"
                , new Color[] {
                    new Color(136, 178, 145),
                    new Color(99, 130, 97)
                });

            biomeData[new biome.ClimateInfo(heat.SubPolarHeat, moisture.VeryDry)]
                 = info;
            biomeData[new biome.ClimateInfo(heat.SubPolarHeat, moisture.Dry)]
                 = info;
            biomeData[new biome.ClimateInfo(heat.SubPolarHeat, moisture.MediumDry)]
                 = info;

            #endregion

            #region DryScrubland

            info = new biome.BiomeInfo("DryScrubland"
                , new Color[] {
                    new Color(166, 206, 141),
                    new Color(182, 206, 140)
                });

            biomeData[new biome.ClimateInfo(heat.BorealHeat, moisture.VeryDry)]
                 = info;

            #endregion

            #region DesertScrubland

            info = new biome.BiomeInfo("DesertScrubland"
                , new Color[] {
                    new Color(221, 255, 130),
                    new Color(232, 255, 130)
                });

            biomeData[new biome.ClimateInfo(heat.TemperateColdHeat, moisture.VeryDry)]
                 = info;
            biomeData[new biome.ClimateInfo(heat.TemperateWarmHeat, moisture.VeryDry)]
                 = info;
            biomeData[new biome.ClimateInfo(heat.TropicalHeat, moisture.VeryDry)]
                 = info;

            #endregion

            #region Scrubland

            info = new biome.BiomeInfo("Scrubland"
                , new Color[] {
                    new Color(214, 255, 132)
                });

            biomeData[new biome.ClimateInfo(heat.TemperateWarmHeat, moisture.Dry)]
                 = info;
            biomeData[new biome.ClimateInfo(heat.TropicalHeat, moisture.Dry)]
                 = info;

            #endregion

            #region Steppe

            info = new biome.BiomeInfo("Steppe"
                , new Color[] {
                    new Color(123, 207, 143),
                    new Color(123, 207, 167)
                });

            biomeData[new biome.ClimateInfo(heat.TemperateColdHeat, moisture.Dry)]
                 = info;

            #endregion

            #region TemperateForest

            info = new biome.BiomeInfo("TemperateForest"
                , new Color[] {
                    new Color(64, 208, 144),
                    new Color(50, 188, 101)
                });

            biomeData[new biome.ClimateInfo(heat.BorealHeat, moisture.Dry)]
                 = info;
            biomeData[new biome.ClimateInfo(heat.TemperateColdHeat, moisture.MediumDry)]
                 = info;
            biomeData[new biome.ClimateInfo(heat.TemperateWarmHeat, moisture.LittleDry)]
                 = info;
            biomeData[new biome.ClimateInfo(heat.TropicalHeat, moisture.Wet)]
                 = info;

            #endregion

            #region DryForest

            info = new biome.BiomeInfo("DryForest"
                , new Color[] {
                    new Color(106, 235, 141),
                    new Color(153, 235, 106)
                });

            biomeData[new biome.ClimateInfo(heat.TemperateWarmHeat, moisture.MediumDry)]
                 = info;
            biomeData[new biome.ClimateInfo(heat.TropicalHeat, moisture.LittleDry)]
                 = info;

            #endregion

            #region VeryDryForest

            info = new biome.BiomeInfo("VeryDryForest"
                , new Color[] {
                    new Color(141, 255, 136),
                    new Color(207, 255, 136)
                });

            biomeData[new biome.ClimateInfo(heat.TropicalHeat, moisture.MediumDry)]
                 = info;

            #endregion

            #region WetForest

            info = new biome.BiomeInfo("WetForest"
                , new Color[] {
                    new Color(0, 208, 162),
                    new Color(0, 208, 110)
                });

            biomeData[new biome.ClimateInfo(heat.BorealHeat, moisture.MediumDry)]
                 = info;
            biomeData[new biome.ClimateInfo(heat.TemperateColdHeat, moisture.LittleDry)]
                 = info;
            biomeData[new biome.ClimateInfo(heat.TemperateWarmHeat, moisture.Wet)]
                 = info;
            biomeData[new biome.ClimateInfo(heat.TropicalHeat, moisture.VeryWet)]
                 = info;

            #endregion

            #region RainForest

            info = new biome.BiomeInfo("RainForest"
                , new Color[] {
                    new Color(0, 207, 208),
                    new Color(0, 208, 110)
                });

            biomeData[new biome.ClimateInfo(heat.BorealHeat, moisture.LittleDry)]
                 = info;
            biomeData[new biome.ClimateInfo(heat.TemperateColdHeat, moisture.Wet)]
                 = info;

            #endregion

            #region TropicalForest

            info = new biome.BiomeInfo("TropicalForest"
                , new Color[] {
                    new Color(0, 255, 131)
                });

            biomeData[new biome.ClimateInfo(heat.TemperateWarmHeat, moisture.VeryWet)]
                 = info;
            biomeData[new biome.ClimateInfo(heat.TropicalHeat, moisture.OverWet)]
                 = info;

            #endregion
        }

        /// <summary>
        /// Get biome correspond with info
        /// </summary>
        /// <param name="climate"></param>
        /// <returns></returns>
        public static biome.Biome BuildBiome(biome.ClimateInfo climate)
        {
            biome.BiomeInfo info;

            if (biomeData.ContainsKey(climate))
            {
                info = biomeData[climate];
            }
            else
            {
                info = biomeData[FindTheNearestClimate(climate)];
            }

            return new biome.Biome(info, climate);
        }

        private static biome.ClimateInfo FindTheNearestClimate(biome.ClimateInfo climate)
        {
            biome.ClimateInfo result = climate;

            while (!biomeData.ContainsKey(result))
            {
                result = new biome.ClimateInfo
                    (
                        temperature: result.Temperature,
                        moisture:(moisture)(((int)result.Moisture) - 1)
                    );
            }

            return result;
        }


    }
}
