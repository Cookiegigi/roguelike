﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlatRedBall;
using Game.DataType.WorldType;
using Game.DataType.ZoneType;
using Game.DataType.ZoneType.Biomes;
using SharpNoise.Modules;
using Utility.ToolBox;
using SharpNoiseModule_;
using Game.Generators;

namespace Game.Generators.WorldGenerators
{
    /// <summary>
    /// Biome generator using perlin noise
    /// </summary>
    public class BiomeGeneratorWithNoise : BiomeGenerator
    {
        //private Module heightNoiseGenerator = new Floor()
        //{
        //    Source0 = new Clamp()
        //    {
        //        LowerBound = 0,
        //        UpperBound = Data.Zone.Biomes.HeightTypeCount - 1,
        //        Source0 = new ScaleBias()
        //        {
        //            Bias = (Data.Zone.Biomes.HeightTypeCount - 1) / 2,
        //            Scale = (Data.Zone.Biomes.HeightTypeCount - 1) / 2,
        //            Source0 = new Perlin()
        //            {
        //                Seed = 0,
        //                Frequency = 0.1,
        //                OctaveCount = 10,
        //                Persistence = 0.5
        //            }
        //        }
        //    }
        //};

        private Module heatNoiseGenerator = new Round()
        {
            Source0 = new Clamp()
            {
                Source0 = new ScaleBias()
                {
                    Source0 = new Clamp()
                    {
                        Source0 = new ScaleBias()
                        {
                            Source0 = new Perlin()
                            {
                                Seed = TrueRandomGenerator.GetRandomNumber(),
                                Frequency = 0.1,
                                OctaveCount = 10,
                                Persistence = 0.5,
                                Lacunarity = 2,
                                Quality = SharpNoise.NoiseQuality.Best
                            },
                            Bias = 0.2
                        },
                        UpperBound = 1,
                        LowerBound = -1
                    },
                    Bias = (Data.Zone.Biomes.HeatTypeCount - 1)/2,
                    Scale = (Data.Zone.Biomes.HeatTypeCount - 1) / 2
                },
                LowerBound = 0,
                UpperBound = Data.Zone.Biomes.HeatTypeCount
            }
        };

        private Module moistureNoiseGenerator = new Round()
        {
            Source0 = new Clamp()
            {
                Source0 = new ScaleBias()
                {
                    Source0 = new Clamp()
                    {
                        Source0 = new ScaleBias()
                        {
                            Source0 = new Perlin()
                            {
                                Seed = TrueRandomGenerator.GetRandomNumber(),
                                Frequency = 0.1,
                                OctaveCount = 10,
                                Persistence = 0.1,
                                Lacunarity = 3,
                                Quality = SharpNoise.NoiseQuality.Best
                            },
                            Bias = 0.2
                        },
                        LowerBound = -1,
                        UpperBound = 1
                    },
                    Bias = (Data.Zone.Biomes.MoistureTypeCount - 1) / 2,
                    Scale = (Data.Zone.Biomes.MoistureTypeCount - 1) / 2
                },
                LowerBound = 0,
                UpperBound = Data.Zone.Biomes.MoistureTypeCount
            }
        };

        public BiomeGeneratorWithNoise(World world) : base(world)
        {

        }

        public override Biome GenerateBiomeForNewZone(Adress adress)
        {
            Biome result = null;

            //Data.Zone.Biomes.HeightType height 
               // = (Data.Zone.Biomes.HeightType)heightNoiseGenerator.GetValue(adress.X, adress.Y, 0);
            Data.Zone.Biomes.HeatType heat = 
                (Data.Zone.Biomes.HeatType)heatNoiseGenerator.GetValue(adress.X, adress.Y, 0);
            Data.Zone.Biomes.MoistureType moisture =
                (Data.Zone.Biomes.MoistureType)moistureNoiseGenerator.GetValue(adress.X, adress.Y, 0);

            result = BiomeFactory.BuildBiome(new ClimateInfo(heat, moisture));

            return result;
        }
    }
}
