﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Game.DataType.ZoneType.Biomes;
using Game.DataType.WorldType;
using Game.DataType.ZoneType;
using Utility.ToolBox;
using FlatRedBall;

namespace Game.Generators.WorldGenerators
{
    public abstract class BiomeGenerator
    {

        public World CurrentWorld { get; set; }

        public BiomeGenerator(World world)
        {
            CurrentWorld = world;
        }

        public abstract Biome GenerateBiomeForNewZone(Adress adress);

    }
}
