﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Game.DataType.ZoneType.Biomes;
using Game.DataType.WorldType;
using Game.DataType.ZoneType;
using Utility.ToolBox;
using Game.Generators.ZoneGenerators;
using Game.DataType.ZoneType.Layers;

namespace Game.Generators.WorldGenerators
{
    /// <summary>
    /// Handle the generation of world
    /// </summary>
    public class WorldGenerator
    {
        public World CurrentWorld { get; set; }

        private BiomeGenerator biomeGenerator;
        private ZoneGenerator zoneGenerator;

        public WorldGenerator(World world)
        {
            CurrentWorld = world;

            biomeGenerator = new BiomeGeneratorWithNoise(world);
            zoneGenerator = new ZoneGenerator(CurrentWorld.Seed);

        }

        public Zone GenerateNewZone(Adress adress)
        {
            

            Zone result = new Zone(adress, biomeGenerator.GenerateBiomeForNewZone(adress));

            zoneGenerator.InitZoneGraph(result);

            result.Layers.Add(new FloorLayer(zoneGenerator, result));
            result.Layers.Add(new PathLayer(zoneGenerator, result));

            return result;
        }

        public void Pregenerate(int nbCase)
        {
            for(int i = 1; i < nbCase; i++)
            {
                CurrentWorld.GenerateNewZone(new Adress(MathToolBox.GetSpiralCoord(i)));
            }
        }
    }
}