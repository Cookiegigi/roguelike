﻿//// obsolete

//using FlatRedBall;
//using Game.DataType.WorldType;
//using Game.DataType.ZoneType;
//using Game.DataType.ZoneType.Biomes;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using Utility.ToolBox;

//namespace Game.Generators.WorldGenerators
//{
//    public class BiomeGeneratorWithoutChunk : BiomeGenerator
//    {
//        private double changeBiomeRate = 10;

//        public BiomeGeneratorWithoutChunk(World world) : base(world)
//        {

//        }

//        public override Biome GenerateBiomeForNewZone(Adress adress)
//        {
//            Biome[] biomes = CurrentWorld.GetAdjacentZoneBiomeOfAnAdress(adress);

//            Biome result = null;

//            Type uniqueTypeInBiomes = ArrayToolBox.IsArrayFullWithUniqueType(biomes);

//            if (uniqueTypeInBiomes != null)
//            {
//                int numberOfTypeItem = ArrayToolBox.GetNumberofTypeItem(uniqueTypeInBiomes, biomes);

//                if (numberOfTypeItem == 4)
//                {
//                    result = Activator.CreateInstance(uniqueTypeInBiomes) as Biome;
//                }
//                else
//                {
//                    int changeBiome = FlatRedBallServices.Random.Next(0, 100);

//                    if (changeBiome < changeBiomeRate * (4 - numberOfTypeItem))
//                    {
//                        result = GetRandomBiomeExcept(new Type[] { uniqueTypeInBiomes });
//                    }
//                    else
//                    {
//                        result = Activator.CreateInstance(uniqueTypeInBiomes) as Biome;
//                    }
//                }
//            }
//            else
//            {
//                Type[] differentTypeOfBiome;
//                int[] numberOfEachBiome = ArrayToolBox.GetOccurenceOfEachType(biomes
//                    , out differentTypeOfBiome);
//                if(differentTypeOfBiome.Length == 0)
//                {
//                    Type type = ArrayToolBox.GetRandomItem(Data.Zone.Biomes.BiomesList.ToArray()
//                        , FlatRedBallServices.Random);
//                    result = Activator.CreateInstance(type) as Biome;

//                }
//                else
//                {
//                    int chance = FlatRedBallServices.Random.Next(0, 100);

//                    if (!ContainsAllBiome(differentTypeOfBiome)
//                        && chance < changeBiomeRate * (4 - differentTypeOfBiome.Length))
//                    {
//                        result = GetRandomBiomeExcept(differentTypeOfBiome);
//                    }
//                    else
//                    {
//                        Type type = differentTypeOfBiome[0];
//                        int chanceResult = 0;
//                        int temp;

//                        for (int i = 0; i < differentTypeOfBiome.Length; i++)
//                        {
//                            temp = FlatRedBallServices.Random.Next(0, 100) * numberOfEachBiome[i];
//                            if (temp > chanceResult)
//                            {
//                                type = differentTypeOfBiome[i];
//                            }
//                        }

//                        result = Activator.CreateInstance(type) as Biome;
//                    }
//                }

//            }

//            return result;
//        }

//        //private Biome[] GetEffectiveAdjacentBiome(Biome[] biomes)
//        //{
//        //    Biome[] temp = new Biome[4];

//        //    for (int i = 0; i < biomes.Length; i++)
//        //    {
//        //        if (biomes[i] != null)
//        //        {
//        //            if (biomes[i] is BiomeBorder)
//        //            {
//        //                temp[i] = ((BiomeBorder)biomes[i]).AdjacentBiome[(i + 2) % 4];
//        //            }
//        //            else
//        //            {
//        //                temp[i] = biomes[i];
//        //            }
//        //        }
//        //    }

//        //    return temp;
//        //}

//        private Biome GetRandomBiomeExcept(Type[] blackList)
//        {
//            List<Type> biomes = Data.Zone.Biomes.BiomesList.ToList();

//            foreach (Type type in blackList)
//            {
//                biomes.Remove(type);
//            }

//            Type newBiome = ArrayToolBox.GetRandomItem(biomes.ToArray(), FlatRedBallServices.Random);

//            return (Biome)Activator.CreateInstance(newBiome);
//        }

//        private bool ContainsAllBiome(Type[] biomes)
//        {
//            bool test = true;
//            List<Type> allBiomes = Data.Zone.Biomes.BiomesList.ToList();

//            foreach (Type type in allBiomes)
//            {
//                if (!biomes.Contains(type))
//                {
//                    test = false;
//                }
//            }

//            return test;
//        }
//    }
//}
