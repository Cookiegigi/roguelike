﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DelaunayVoronoi;
using FlatRedBall;
using FlatRedBall.Math;
using Game.DataType.ZoneType;
using Microsoft.Xna.Framework;

namespace Game.Generators.ZoneGenerators
{
    /// <summary>
    /// Handle zone layer generation
    /// </summary>
    public class ZoneGenerator
    {
        private FloorGenerators floorGenerators;
        private PathGenerator pathGenerator;

        private int[] zoneSize;

        private int[] ZoneSize
        {
            get
            {
                if(zoneSize == null)
                {
                    int[] temp = Screens.ZoneScreen.SizeOfZone;
                    int[] result = new int[2];

                    int[] result1 = new int[2];

                    MathFunctions.AbsoluteToWindow(-temp[0], -temp[1]
                        , 0, ref result1[0], ref result1[1], Camera.Main);

                    int[] result2 = new int[2];

                    MathFunctions.AbsoluteToWindow(temp[0], temp[1]
                        , 0, ref result2[0], ref result2[1], Camera.Main);

                    result[0] = result2[0] - result1[0];
                    result[1] = -(result2[1] - result1[1]);

                    zoneSize = result;
                }
                
                return zoneSize;
            }
        }

        private int Seed;
        private RandomGenerator rng;

        //seed bug : - 890457612
        public ZoneGenerator(int seed)
        {
            floorGenerators = new FloorVoronoiGraph( seed);
            pathGenerator = new PathGeneratorV1(seed);
        }

        public void InitZoneGraph(Zone zone)
        {
            int seed = Seed ^ zone.ZoneAdress.X ^ zone.ZoneAdress.Y;

            rng = new RandomGenerator(seed, 0.1);

            GenerateGraphs(ZoneSize[0], ZoneSize[1], zone, seed);
        }

        private void GenerateGraphs(int maxX, int maxY, Zone zone, int seed)
        {
            //number of point in delaunay graph
            //TODO : change number of point randomly
            int nbPoint = rng.Next(20, 40);

            // generate delaunay graph
            if (zone.GraphDelaunay == null)
            {
                zone.GraphDelaunay = new DelaunayTriangulator(nbPoint, maxX, maxY, seed);
            }

            //generate voronoi edges
            if (zone.GraphVoronoi == null)
            {
                zone.GraphVoronoi = new DelaunayVoronoi.Voronoi(zone.GraphDelaunay);
            }
        }

        public Color[,] GetZoneFloor(Zone zone)
        {
            Color[,] result = new Color[ZoneSize[0], ZoneSize[1]];

            floorGenerators.GetFloorColors(ref result, zone);

            return result;
        }

        public Color[,] GetZonePath(Zone zone)
        {
            Color[,] result = new Color[ZoneSize[0], ZoneSize[1]];

            pathGenerator.GetPathColor(ref result, zone);

            return result;
        }
    }
}
