﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Game.DataType.ZoneType;
using Microsoft.Xna.Framework;
using Utility.ToolBox;
using DelaunayVoronoi;
using Utility.DataType;

namespace Game.Generators.ZoneGenerators
{
    public class PathGeneratorV1 : PathGenerator
    {

        public static Color[] colors = new Color[] { new Color(228, 202, 176), new Color(240, 215, 192), new Color(211, 171, 135) };

        public static Random rng = new Random();

        public PathGeneratorV1(int seed) : base(seed)
        {
        }

        public override void GetPathColor(ref Color[,] array, Zone zone)
        {
            //Calculate path
            DelaunayVoronoi.Point start, stop;

            ChooseStartAndStop(zone, array.GetLength(0), array.GetLength(1), out start, out stop);

            zone.Path.PathPoint = FindPathPoint(zone, start, stop);

            DelaunayVoronoi.Point p1, p2;

            //Draw the path on layer
            for(int i = 0; i < zone.Path.PathPoint.Count-1; i++)
            {
                p1 = zone.Path.PathPoint[i];
                p2 = zone.Path.PathPoint[i + 1];

                DrawingToolBox.DrawLineThickWithMultipleColor(
                    array,
                    colors,
                    p1.ToIntegerArray(),
                    p2.ToIntegerArray(),
                    10
                    );
            }
        }

        private List<DelaunayVoronoi.Point> FindPathPoint(Zone zone, DelaunayVoronoi.Point start, DelaunayVoronoi.Point stop)
        {

            List<DelaunayVoronoi.Point> temp = zone.GraphDelaunay.Points;
            temp.Add(start);
            temp.Add(stop);

            PathFinder pathFinder = new PathFinder(ConvertPointsToNode(temp));

            pathFinder.Process(
                pathFinder.Nodes.Find(n => n.X == start.X && n.Y == start.Y),
                pathFinder.Nodes.Find(n => n.X == stop.X && n.Y == stop.Y)
                );

            List<DelaunayVoronoi.Point> result = new List<DelaunayVoronoi.Point>();
            foreach(DijkstraNode node in pathFinder.Path)
            {
                result.Add(zone.GraphDelaunay.Points[node.Id]);
            }

            return result;
        }

        private List<DijkstraNode> ConvertPointsToNode(List<DelaunayVoronoi.Point> points)
        {
            List<DijkstraNode> result = new List<DijkstraNode>();

            for(int i = 0; i < points.Count; i++)
            {
                result.Add(new DijkstraNode(points[i].X, points[i].Y, i));
            }

            List<DijkstraNode> temp;
            DelaunayVoronoi.Point pTemp;

            for(int i = 0; i < result.Count; i++)
            {
                temp = new List<DijkstraNode>();

                foreach(Edge e in points[i].AdjacentEdges)
                {
                    pTemp = (e.Point1 != points[i]) ? e.Point1 : e.Point2;
                    temp.Add(result.Find(n => n.X == pTemp.X && n.Y == pTemp.Y));
                }
                result[i].Neighbours = temp;
            }

            return result;
        }


        private void ChooseStartAndStop(Zone zone, int sizeX, int sizeY, out DelaunayVoronoi.Point start, out DelaunayVoronoi.Point stop)
        {

            bool[] candidat = new bool[4];

            //Choose depend on neighbourn
            if (zone.North!= null && zone.North.Path.SouthPath)
            {
                candidat[0] = true;
            }
            if (zone.East != null && zone.East.Path.WestPath)
            {
                candidat[1] = true;
            }
            if (zone.South != null && zone.South.Path.NorthPath)
            {
                candidat[2] = true;
            }
            if (zone.West != null && zone.West.Path.EastPath)
            {
                candidat[3] = true;
            }

            int index1, index2;
            // complet candidat
            int temp = candidat.Where(c => c).Count();
            if (temp % 2 != 0 || temp == 0)
            {
                
                do
                {
                    index1 = rng.Next(0, 3);
                } while (candidat[index1]);
                
                do
                {
                    index2 = rng.Next(0, 3);
                } while (index1 == index2);

                candidat[index1] = true;
                if(temp < 1)
                {
                    candidat[index2] = true;
                }
                
            }

            List<DelaunayVoronoi.Point> possible = new List<DelaunayVoronoi.Point>();
            //Find Point

            DelaunayVoronoi.Point point;
            Edge tempEdge;
            if (candidat[0])
            {
                point = new DelaunayVoronoi.Point(sizeX / 2, 0);
                tempEdge = new Edge(point, FindOneEdgeNear(sizeX / 2, 0, zone));
                point.AdjacentEdges.Add(tempEdge);
                possible.Add(point);
            }
            if (candidat[1])
            {
                point = new DelaunayVoronoi.Point(0, sizeY / 2);
                tempEdge = new Edge(point, FindOneEdgeNear(0, sizeY / 2, zone));
                point.AdjacentEdges.Add(tempEdge);
                possible.Add(point);
            }
            if (candidat[2])
            {
                point = new DelaunayVoronoi.Point(sizeX / 2, sizeY);
                tempEdge = new Edge(point, FindOneEdgeNear(sizeX / 2, sizeY, zone));
                point.AdjacentEdges.Add(tempEdge);
                possible.Add(point);
            }
            if (candidat[3])
            {
                point = new DelaunayVoronoi.Point(sizeX, sizeY / 2);
                tempEdge = new Edge(point, FindOneEdgeNear(sizeX, sizeY / 2, zone));
                point.AdjacentEdges.Add(tempEdge);
                possible.Add(point);
            }

            index1 = rng.Next(0, possible.Count);
            do
            {
                index2 = rng.Next(0, possible.Count);
            } while (index1 == index2);

            start = possible[index1];
            stop = possible[index2];
        }

        private DelaunayVoronoi.Point FindOneEdgeNear(int v1, int v2, Zone zone)
        {
            List<double> distances = new List<double>();
            DelaunayVoronoi.Point temp = new DelaunayVoronoi.Point(v1, v2);
            foreach (DelaunayVoronoi.Point p in zone.GraphDelaunay.Points)
            {
                distances.Add(DelaunayVoronoi.Point.Distance(temp, p));
            }

            return zone.GraphDelaunay.Points[distances.IndexOf(distances.Min())];
        }


    }
}
