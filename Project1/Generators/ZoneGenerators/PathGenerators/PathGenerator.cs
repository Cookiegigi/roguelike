﻿using Game.DataType.ZoneType;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Generators.ZoneGenerators
{
    public abstract class PathGenerator
    {
        public int Seed { get; set; }

        public PathGenerator(int seed)
        {
            Seed = seed;
        }

        /// <summary>
        /// Get the path layer pixel array
        /// </summary>
        /// <param name="array"></param>
        /// <param name="zone"></param>
        public abstract void GetPathColor(ref Color[,] array, Zone zone);

    }
}
