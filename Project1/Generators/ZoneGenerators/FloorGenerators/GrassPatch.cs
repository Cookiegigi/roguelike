﻿using DelaunayVoronoi;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Generators.ZoneGenerators
{
    /// <summary>
    /// Not implemented
    /// </summary>
    class GrassPatch : FloorPatch
    {
        public GrassPatch(Polygon p, Color color, int high) : base(p, color, high)
        {

        }

        public override void DrawPatch(Color[,] array, RandomGenerator rng)
        {
            base.DrawPatch(array, rng);

            DrawBorders(array, rng);
        }

        private void DrawBorders(Color[,] array, RandomGenerator rng)
        {
            Polygon p = null;

            const int amplitude = 5;

            foreach (Edge e in Edges)
            {
                p = e.Polygons.Where(pol => pol != this).FirstOrDefault();

                if(p == null)
                {
                    //TODO
                }
                else if(((FloorPatch)p).High == High)
                {
                    //TODO : 
                }
                else if(((FloorPatch)p).High < High)
                {
                    //TODO
                }
            }

        }

        
        private void DrawBorder(Color[,] array, int amplitudeMax, Edge[] edges, RandomGenerator rng)
        {
            DelaunayVoronoi.Point mp, p1, p2, extremPoint;

            //find middle point
            if (edges[0].Point1.AdjacentEdges.Contains(edges[1]))
            {
                mp = edges[0].Point1;
                p1 = edges[0].Point2;
            }
            else
            {
                mp = edges[0].Point2;
                p1 = edges[0].Point1;
            }
            if(edges[1].Point1 == mp)
            {
                p2 = edges[1].Point2;
            }
            else
            {
                p2 = edges[1].Point1;
            }

            //find extremPoint
            double A, B;
            A = p1.X - p2.X;
            B = p1.Y - p2.Y;

            extremPoint = new DelaunayVoronoi.Point
                (
                    -A / (A * A + B * B) + mp.X,
                    A / (A * A + B * B) + mp.Y
                );

            extremPoint *= rng.Next(0, amplitudeMax);

            // calculate controlPoint


        }



        private double[] FindOrthogonalVector(Edge edge, double x)
        {
            const double dx = 0.1;
            double x1, x2, y1, y2, y;
            double[] result = new double[2];

            x1 = x - dx;
            y1 = edge.YOnEdge(x1);

            x2 = x + dx;
            y2 = edge.YOnEdge(x2);

            y = edge.YOnEdge(x);

            double A, B;
            A = x2 - x1;
            B = y2 - y1;

            result[0] = -B / (A * A + B + B) + x;
            result[1] = A / (A * A + B + B) + y;

            double norm = Math.Sqrt(result[0] * result[0] + result[1] * result[1]);

            result[0] = result[0] / norm;
            result[1] = result[1] / norm;

            return result;

        }

    }
}
