﻿using Game.DataType.ZoneType;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Generators.ZoneGenerators
{
    public abstract class FloorGenerators
    {
        public int Seed { get; private set; }

        protected FloorGenerators(int seed)
        {
            Seed = seed;
        }

        /// <summary>
        /// Get the floor layer pixel array
        /// </summary>
        /// <param name="array"></param>
        /// <param name="zone"></param>
        public abstract void GetFloorColors(ref Color[,] array, Zone zone);
    }
}
