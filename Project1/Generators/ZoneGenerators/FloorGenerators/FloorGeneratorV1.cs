﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Game.DataType.ZoneType;
using Microsoft.Xna.Framework;
using SharpNoise.Modules;
using SharpNoiseModule_;
using Utility.ToolBox;

namespace Game.Generators.ZoneGenerators
{
    /// <summary>
    /// Floor generation using voronoi implementation from the SharpNoise
    /// </summary>
    public class FloorGeneratorV1 : FloorGenerators
    {
        public FloorGeneratorV1(int seed) : base(seed)
        {
        }

        public override void GetFloorColors(ref Color[,] array, Zone zone)
        {
            int nbCol = zone.ZoneBiome.General.ColorList.Count();
            Color[] colors = zone.ZoneBiome.General.ColorList;

            double[,] kernel = new double[,]
            {
                {0.04, 0.04, 0.04, 0.04, 0.04 },
                {0.04, 0.04, 0.04, 0.04, 0.04 },
                {0.04, 0.04, 0.04, 0.04, 0.04 },
                {0.04, 0.04, 0.04, 0.04, 0.04 },
                {0.04, 0.04, 0.04, 0.04, 0.04 }
            };

            Module noiseGen =new ScaleBias()
                {
                    Source0 = new ScaleBias()
                    {
                        Source0 = new Clamp()
                        {
                            Source0 = new Voronoi()
                            {
                                Seed = 0,
                                Frequency = 1e-2


                            },
                            LowerBound = -1,
                            UpperBound = 1
                        },
                        Bias = 1,
                        Scale = 1
                    },
                    Bias = 0,
                    Scale = 0.5 * (nbCol - 1)
            };

            double[,] noises = new double[array.GetLength(0), array.GetLength(1)];

            // base
            for (int i = 0
                ; i < array.GetLength(0); i++)
            {
                for (int j = 0
                    ; j < array.GetLength(1); j++)
                {
                    noises[i, j] = noiseGen.GetValue(i, j, 0);
                }
            }

            //filtre
            MathToolBox.ConvulutionProduct(noises, kernel);
            
            


            // baseFilt -> Color
            for (int i = 0; i < array.GetLength(0); i++)
            {
                for (int j = 0; j < array.GetLength(1); j++)
                {
                    int colorIndex = (int)Math.Round(noises[i, j]);

                    array[i, j] = colors[colorIndex];
                }
            }

            //filtre
            //Filter(ref array);

        }

        private void Filter(ref Color[,] array)
        {
            Color[,] result = new Color[array.GetLength(0), array.GetLength(1)];

            List<Color> temp = new List<Color>();


            for (int i = 0; i < array.GetLength(0); i++)
            {
                for (int j = 0; j < array.GetLength(1); j++)
                {
                    if (i != 0)
                    {
                        if (j != 0)
                        {
                            temp.Add(array[i - 1, j - 1]);
                        }

                        temp.Add(array[i - 1, j]);

                        if(j != array.GetLength(1) - 1)
                        {
                            temp.Add(array[i - 1, j + 1]);
                        }
                    }
                        
                    if(j != 0)
                    {
                        temp.Add(array[i, j - 1]);

                        if(i != array.GetLength(0) - 1)
                        {
                            temp.Add(array[i + 1, j - 1]);
                        }
                    }

                    if(i != array.GetLength(0) - 1)
                    {
                        temp.Add(array[i + 1, j]);

                        if(j != array.GetLength(1) - 1)
                        {
                            temp.Add(array[i + 1, j + 1]);
                        }
                    }

                    if(j != array.GetLength(1) - 1)
                    {
                        temp.Add(array[i, j + 1]);
                    }

                    temp.Add(array[i, j]);

                    result[i, j] =  temp.GroupBy(e => e).OrderBy(e => e.Count()).First().First();

                    temp.Clear();
                }
            }

            array = result;
        }
    }
}
