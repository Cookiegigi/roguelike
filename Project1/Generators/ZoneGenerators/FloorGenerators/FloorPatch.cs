﻿using DelaunayVoronoi;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility.ToolBox;

namespace Game.Generators.ZoneGenerators
{
    /// <summary>
    /// Polygon -> Patch
    /// </summary>
    class FloorPatch : Polygon
    {
        public Color PatchColor { get; set; }

        /// <summary>
        /// Order of drawing
        /// </summary>
        public int High { get; set; }

        /// <summary>
        /// not implemented
        /// </summary>
        public int NoisePercentage { get; set; } = 10;

        public HashSet<FloorPatch> AdjacentFloorPatch { get; set; }

        public FloorPatch() : base()
        {

        }

        /// <summary>
        /// Create a patch with polygon
        /// </summary>
        /// <param name="p"></param>
        /// <param name="color"></param>
        /// <param name="high"></param>
        public FloorPatch(Polygon p, Color color, int high) : base(p)
        {
            PatchColor = color;
            AdjacentFloorPatch = new HashSet<FloorPatch>();
            High = high;
        }

        /// <summary>
        /// Draw patch on layer
        /// filll the polygon layer by layer
        /// </summary>
        /// <param name="array"></param>
        /// <param name="rng"></param>
        public virtual void DrawPatch(Color[,] array, RandomGenerator rng)
        {

            var pY1 = this.Edges.Select(e => e.Point1.Y);
            var pY2 = Edges.Select(e => e.Point2.Y);
            var pY = pY1.Concat(pY2);

            int min = (int)pY.Min();
            min = (min >= 0) ? min : 0;
            int max = (int)pY.Max();
            max = (max < array.GetLength(1)) ? max : array.GetLength(1) - 1;

            for (int y = min; y <= max; y++)
            {
                DrawOneLayer(array, y, rng);
            }

            DrawBorder(array);

        }

        private void DrawBorder(Color[,] array)
        {
            Polygon p;

            foreach(Edge e in Edges)
            {
                p = e.Polygons.Where(pol => pol != this).FirstOrDefault();

                if (p == null)
                {
                    DrawingToolBox.DrawLineAntiAliasedThick(array, Color.Black, e.Point1.ToIntegerArray(), e.Point2.ToIntegerArray(),1);
                }
                else if (((FloorPatch)p).High == High)
                {
                    //TODO : 
                }
                else if (((FloorPatch)p).High < High)
                {
                    DrawingToolBox.DrawLineAntiAliasedThick(array, Color.Black, e.Point1.ToIntegerArray(), e.Point2.ToIntegerArray(), 1);
                }

               
            }
        }


        private void DrawOneLayer(Color[,] array, int y, RandomGenerator rng
            , int minNoise = -10, int maxNoise = 10)
        {
            

            var points = IntersectWithHorizontal(y);

            DelaunayVoronoi.Point start, stop;

            start = points.Where(p => p.X == points.Select(p1 => p1.X).Min()).First();
            stop = points.Where(p => p.X == points.Select(p1 => p1.X).Max()).First();

            double rn = 0;

            for (int x = (int)start.X + 1; x < stop.X; x++)
            {
                if (x >= 0 && x < array.GetLength(0))
                {
                    

                    if ( rn >= NoisePercentage)
                    {
                        array[x, y] = Color.Black;
                    }
                    else
                    {
                        array[x, y] = PatchColor;
                    }
                    
                }
            }
        }

        private List<DelaunayVoronoi.Point> IntersectWithHorizontal(int y)
        {
            var edges = Edges.Where((e) =>
           {
               int min, max;
               min = Math.Min((int)e.Point1.Y, (int)e.Point2.Y);
               max = Math.Max((int)e.Point1.Y, (int)e.Point2.Y);

               return y >= min && y <= max;
           }).ToList();

            List<DelaunayVoronoi.Point> result = new List<DelaunayVoronoi.Point>(); ;

            foreach (Edge e in edges)
            {
                result.Add(new DelaunayVoronoi.Point(e.XOnEdge(y), y));
            }

            return result;
        }
    }
}
