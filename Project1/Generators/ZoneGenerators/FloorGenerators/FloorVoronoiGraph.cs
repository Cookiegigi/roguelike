﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DelaunayVoronoi;
using Game.DataType.ZoneType;
using Microsoft.Xna.Framework;
using SharpNoise.Modules;
using Utility.ToolBox;

namespace Game.Generators.ZoneGenerators
{

    /// <summary>
    /// Floor generation using voronoi generated with my implementation
    /// </summary>
    public class FloorVoronoiGraph : FloorGenerators
    {
        private DelaunayVoronoi.Voronoi voronoi;
        private DelaunayTriangulator delaunay;

        private List<FloorPatch> patches;


        private Color DELAUNAYPOINTCOLOR = Color.Red;
        private Color DELAUNAYTRIANGLECOLOR = Color.Red;
        private Color VORONOIEDGECOLOR = Color.Blue;
        private Color VORONOIPOLYCOLOR = Color.Blue;
        private Color VORONOIPOINTCOLOR = Color.Blue;

        private RandomGenerator rng, rng2;

        public FloorVoronoiGraph(int seed) : base(seed)
        {
            
        }

        public FloorVoronoiGraph() : this(0)
        {
            
        }

        public override void GetFloorColors(ref Color[,] array, Zone zone)
        {
            patches = new List<FloorPatch>();

            int seed = Seed ^ zone.ZoneAdress.X ^ zone.ZoneAdress.Y;

            rng = new RandomGenerator(seed, 0.1);
            rng2 = new RandomGenerator(seed + 1, 1);

            // zone color
            int nbCol = zone.ZoneBiome.General.ColorList.Count();
            Color[] colors = zone.ZoneBiome.General.ColorList;

            //Generate Graphs
            GenerateGraphs(array.GetLength(0) - 1, array.GetLength(1) - 1, zone, seed);

            DrawPatches(array);
            //DrawOnePatche(array, patches[6]);

            //Draw graph
            //DrawDelaunayPoints(array);
            //DrawDelaunayTriangles(array);
            //DrawVoronoiEdges(array);
            //DrawVoronoiPoints(array);
            //DrawPolygons(array);
            //DrawOnePolygon(array, patches[10]);

        }

        

        private void GenerateGraphs(int maxX, int maxY, Zone zone, int seed)
        {
           
            // generate delaunay graph
            delaunay = zone.GraphDelaunay;

            //generate voronoi edges
            voronoi = zone.GraphVoronoi;

            //create patches
            var colors = zone.ZoneBiome.General.ColorList;
            int rn1, rn2 ;
            
            foreach (Polygon p in zone.GraphVoronoi.Polygons)
            {


                rn1 = rng.Next(0, colors.Count(), (int)p.Center.X, (int)p.Center.Y);
                rn2 = rng.Next(0, 10, (int)p.Center.X, (int)p.Center.Y);

                if(zone.ZoneBiome.General.Name != "Desert"&&
                    zone.ZoneBiome.General.Name != "DryTundra" &&
                    zone.ZoneBiome.General.Name != "PolarDesert" &&
                    zone.ZoneBiome.General.Name != "DesertScrubland" &&
                    zone.ZoneBiome.General.Name != "DryScrubland")
                {
                    patches.Add(new GrassPatch(p, colors[rn1], rn2));
                }
                else
                {
                    patches.Add(new FloorPatch(p, colors[rn1], rn2));
                }
                
            }

            // smooth patches
            foreach(FloorPatch f in patches)
            {
                f.Smooth(rng.Next(5, 20, (int)f.Center.X, (int)f.Center.Y));
            }

        }

        private void DrawPoints(Color[,] array
            , IEnumerable<DelaunayVoronoi.Point> points
            , Color color)
        {
            foreach (DelaunayVoronoi.Point p in points)
            {
                DrawingToolBox.DrawDisc(array, color
                    , new int[] { (int)p.X, (int)p.Y }, 3);
            }
        }

        private void DrawDelaunayPoints(Color[,] array)
        {
            DrawPoints(array, delaunay.Points, DELAUNAYPOINTCOLOR);
        }
        private void DrawVoronoiPoints(Color[,] array)
        {
            DrawPoints(array, voronoi.Points, VORONOIPOINTCOLOR);
        }

        private void DrawDelaunayTriangles(Color[,] array)
        {
            foreach (Triangle triangle in delaunay.Triangles)
            {
                DrawingToolBox.DrawLine(array, DELAUNAYTRIANGLECOLOR,
                    triangle.Vertices[0].ToIntegerArray(),
                    triangle.Vertices[1].ToIntegerArray());
                DrawingToolBox.DrawLine(array, DELAUNAYTRIANGLECOLOR,
                    triangle.Vertices[1].ToIntegerArray(),
                    triangle.Vertices[2].ToIntegerArray());
                DrawingToolBox.DrawLine(array, DELAUNAYTRIANGLECOLOR,
                    triangle.Vertices[2].ToIntegerArray(),
                    triangle.Vertices[0].ToIntegerArray());
            }
        }

        private void DrawVoronoiEdges(Color[,] array)
        {
            foreach (Edge edge in voronoi.Edges)
            {
                DrawingToolBox.DrawLine(array, VORONOIEDGECOLOR,
                    edge.Point1.ToIntegerArray(), edge.Point2.ToIntegerArray());
            }
        }

        private void DrawPolygons(Color[,] array)
        {
            foreach (Polygon p in patches)
            {
                foreach (Edge edge in p.Edges)
                {
                    DrawingToolBox.DrawLine(array, VORONOIPOLYCOLOR,
                        edge.Point1.ToIntegerArray(), edge.Point2.ToIntegerArray());
                }
            }
        }

        private void DrawOnePolygon(Color[,] array, Polygon p)
        {
            foreach (Edge edge in p.Edges)
            {
                DrawingToolBox.DrawLine(array, Color.Red,
                    edge.Point1.ToIntegerArray(), edge.Point2.ToIntegerArray());
            }
        }

        private void DrawPatches(Color[,] array)
        {
            foreach(FloorPatch p in patches.OrderBy(p => p.High))
            {
                p.DrawPatch(array, rng);
            }
        }

        private void DrawOnePatche(Color[,] array, FloorPatch p)
        {
            p.DrawPatch(array, rng);
        }
    }
}
