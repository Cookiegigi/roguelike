﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Game.Generators
{
    /// <summary>
    /// Random generator using electrical noise from component
    /// </summary>
    public static class TrueRandomGenerator
    {


        public static int GetRandomNumber()
        {
            using(RNGCryptoServiceProvider rg = new RNGCryptoServiceProvider())
            {
                byte[] rno = new byte[5];
                rg.GetBytes(rno);
                return BitConverter.ToInt32(rno, 0);
            }
        }

       

    }
}
