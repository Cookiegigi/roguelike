﻿using SharpNoise.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Generators
{
    /// <summary>
    /// Pseudo random generator using perlin noise
    /// </summary>
    public class RandomGenerator
    {
        private int nextInt = 0;

        public RandomGenerator(int seed, double frequency)
        {
            Seed = seed;

            RNG = new Clamp()
            {
                Source0 = new Perlin()
                {
                    Seed = seed,
                    Frequency = frequency
                },
                LowerBound = -1,
                UpperBound = 1
            };
        }

        public int Seed { get; private set; }
        public Module RNG { get; private set; }

        public int Next(int min, int max, int x, int y)
        {
            return (int)Math.Round((min + ((max - 1) - min) * (RNG.GetValue(x, y, 0) + 1)/2));
        }

        public int Next(int min, int max)
        {
            return Next(min, max, nextInt++, 0);
        }
    }
}
