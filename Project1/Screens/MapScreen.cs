using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

using FlatRedBall;
using FlatRedBall.Input;
using FlatRedBall.Instructions;
using FlatRedBall.AI.Pathfinding;
using FlatRedBall.Graphics.Animation;
using FlatRedBall.Graphics.Particle;
using FlatRedBall.Math.Geometry;
using FlatRedBall.Localization;
using Game.DataType.WorldType;
using Game.Entities.MapEntity;
using Game.DataType.ZoneType;
using Game.Data.Global;
using Microsoft.Xna.Framework;

using heat = Game.Data.Zone.Biomes.HeatType;
using moisture = Game.Data.Zone.Biomes.MoistureType;

namespace Game.Screens
{
    /// <summary>
    /// Screen where map of all zone are display
    /// </summary>
	public partial class MapScreen
	{

        public World Zones { get; set; }

        private Zone playerLastZone;

        private float TileSizeX { get; set; }
        private float TileSizeY { get; set; }

        private float tileSizeXMax;
        private float tileSizeXMin;

        private float tileSizeYMax;
        private float tileSizeYMin;

        private float centerX;
        private float centerY;

        private float offset;

        private float cameraMoveSpeed = 500;

        private IPressableInput closeMap;
        private I2DInput cameraMove;
        private I1DInput zoomMove;
        private IPressableInput switchMode;

        private float ZoomXPercent
        {
            get
            {
                return (TileSizeX - tileSizeXMin) / (tileSizeXMax - tileSizeXMin);
            }
            set
            {
                TileSizeX = value * (tileSizeXMax - tileSizeXMin) + tileSizeXMin;
            }
        }

        private float ZoomYPercent
        {
            get
            {
                return (TileSizeY - tileSizeYMin) / (tileSizeYMax - tileSizeYMin);
            }
            set
            {
                TileSizeY = value * (tileSizeYMax - tileSizeYMin) + tileSizeYMin;
            }
        }

        private Color[] gradientColorTemperature;
        private Color[] gradientColorMoisture;

        /// <summary>
        /// 3 different mode for the map
        /// tab for change
        /// </summary>
        enum MapMode
        {
            Normal,
            HeatMap,
            MoistureMap
        }

        private MapMode currentMode;
        private bool mapModeUpdate = false;

        private MapMode CurrentMode
        {
            get
            {
                return currentMode;
            }
            set
            {
                currentMode = value;
                mapModeUpdate = true;
            }
        }
        

        void CustomInitialize()
		{
            if(Zones == null)
            {
                Zones = GlobalWorld.CurrentWorld;
            }

            GradientInit();

            centerX = Zones.CurrentZone.ZoneAdress.X;
            centerY = Zones.CurrentZone.ZoneAdress.Y;

            offset = 10;

            TileSizeX = 80;
            TileSizeY = 60;

            Camera camera = SpriteManager.Camera;
            tileSizeXMax = camera.AbsoluteRightXEdgeAt(camera.Z)/2;
            tileSizeXMin = 50;
            tileSizeYMax = camera.AbsoluteTopYEdgeAt(camera.Z)/2;
            tileSizeYMin = 40;
            

            closeMap = InputManager.Keyboard.GetKey(KeysMap.Map["Close Map"]);
            switchMode = InputManager.Keyboard.GetKey(KeysMap.Map["Switch Mode"]);

            cameraMove = cameraMove = InputManager.Keyboard.Get2DInput
                (
                Data.Global.KeysMap.Map["Left"],
                Data.Global.KeysMap.Map["Right"],
                Data.Global.KeysMap.Map["Up"],
                Data.Global.KeysMap.Map["Down"]
                );
            zoomMove = InputManager.Mouse.ScrollWheel;

            CurrentMode = MapMode.Normal;

        }

        private void GradientInit()
        {

            #region Temperature gradient
            gradientColorTemperature = new Color[Data.Zone.Biomes.HeatTypeCount];
            
            gradientColorTemperature[(int)heat.PolarHeat] = new Color(0, 0, 255);
            gradientColorTemperature[(int)heat.SubPolarHeat] = new Color(0, 140, 255);
            gradientColorTemperature[(int)heat.BorealHeat] = new Color(0, 255, 255);
            gradientColorTemperature[(int)heat.TemperateColdHeat] = new Color(0, 255, 90);
            gradientColorTemperature[(int)heat.TemperateWarmHeat] = new Color(120, 255, 0);
            gradientColorTemperature[(int)heat.TropicalHeat] = new Color(255, 180, 0);

            #endregion

            #region Moisture gradient

            gradientColorMoisture = new Color[Data.Zone.Biomes.MoistureTypeCount];

            gradientColorMoisture[(int)moisture.OverDry] = new Color(255, 0, 0);
            gradientColorMoisture[(int)moisture.VeryDry] = new Color(255, 90, 0);
            gradientColorMoisture[(int)moisture.Dry] = new Color(255, 255, 0);
            gradientColorMoisture[(int)moisture.MediumDry] = new Color(190, 255, 0);
            gradientColorMoisture[(int)moisture.LittleDry] = new Color(150, 255, 0);
            gradientColorMoisture[(int)moisture.Wet] = new Color(0, 255, 0);
            gradientColorMoisture[(int)moisture.VeryWet] = new Color(0, 255, 90);
            gradientColorMoisture[(int)moisture.OverWet] = new Color(0, 0, 255);

            #endregion
        }

        void CustomActivity(bool firstTimeCalled)
		{
            ZoomActivity();
            TileActivity();

            if (closeMap.IsDown)
            {
                MoveToScreen(typeof(ZoneScreen));
            }

            if (switchMode.WasJustPressed)
            {
                CurrentMode = (MapMode)(
                    ((int)CurrentMode + 1) % Enum.GetNames(typeof(MapMode)).Length
                    );
            }

            Camera camera = Camera.Main;

            camera.XVelocity = cameraMoveSpeed * cameraMove.X;
            camera.YVelocity = cameraMoveSpeed * cameraMove.Y;
        }

        private void ZoomActivity()
        {

            if(zoomMove.Velocity != 0)
            {
                float temp = (zoomMove.Velocity) / 100;

                if (ZoomXPercent + temp > 0
                && ZoomXPercent + temp < 1
                && ZoomYPercent + temp > 0
                && ZoomYPercent + temp < 1)
                {

                    ZoomXPercent += temp;
                    ZoomYPercent += temp;
                    mapModeUpdate = true;
                    ResetMap();
                    playerLastZone = null;
                }
            }
            
        }

        private void ResetMap()
        {
            foreach(Tile tile in Tiles)
            {
                tile.RemoveFromManagers();
            }
            Tiles.Clear();
        }

        private void TileActivity()
        {
            Tile temp;

            foreach(Zone zone in Zones.Zones)
            {
                temp = ContainsTileWithZone(zone);
                if(temp == null)
                {
                    DrawNewTile(zone);
                }
            }

            if(playerLastZone != Zones.CurrentZone)
            {
                if (playerLastZone != null)
                {
                    ContainsTileWithZone(playerLastZone).PlayerHere = false;
                }
                ContainsTileWithZone(Zones.CurrentZone).PlayerHere = true;
                playerLastZone = Zones.CurrentZone;
            }

            if (mapModeUpdate)
            {
                if(CurrentMode == MapMode.Normal)
                {
                    SwitchMapModeToNormal();
                }
                else if(CurrentMode == MapMode.HeatMap)
                {
                    SwitchMapModeToHeat();
                }
                else if(CurrentMode == MapMode.MoistureMap)
                {
                    SwitchMapModeToMoisture();
                }


                mapModeUpdate = false;
            }
            
            
        }

        private void SwitchMapModeToMoisture()
        {
            foreach (Tile tile in Tiles)
            {
                tile.AxisAlignedRectangleInstance.Color =
                    gradientColorMoisture
                    [(int)tile.AssociatedZone.ZoneBiome.Climate.Moisture];

                tile.AdressDisplayText = tile.AssociatedZone.ZoneBiome.Climate.ContractMoisture;
            }
        }

        private void SwitchMapModeToHeat()
        {
            foreach(Tile tile in Tiles)
            {
                tile.AxisAlignedRectangleInstance.Color =
                    gradientColorTemperature
                    [(int)tile.AssociatedZone.ZoneBiome.Climate.Temperature];

                tile.AdressDisplayText = tile.AssociatedZone.ZoneBiome.Climate.ContractTemperature;
            }
        }

        private void SwitchMapModeToNormal()
        {
            foreach(Tile tile in Tiles)
            {
                tile.AxisAlignedRectangleInstance.Color =
                    tile.AssociatedZone.ZoneBiome.General.MainColor;
                tile.AdressDisplayText = tile.AssociatedZone.ZoneBiome.General.ContractName;
            }
        }

        private void DrawNewTile(Zone zone)
        {
            Tile tile = new Tile();
            tile.AssociatedZone = zone;

            Tiles.Add(tile);
            tile.X = 2*(zone.ZoneAdress.X + 1) * (TileSizeX + offset) + centerX;
            tile.Y = 2*(zone.ZoneAdress.Y + 1) * (TileSizeY + offset) + centerY;

            tile.AxisAlignedRectangleInstance.ScaleX = TileSizeX;
            tile.AxisAlignedRectangleInstance.ScaleY = TileSizeY;

            //tile.AxisAlignedRectangleInstance.Color = zone.ZoneBiome.General.MainColor;

            //tile.AdressDisplayText = zone.ZoneBiome.General.ContractName;
        }

        private Tile ContainsTileWithZone(Zone zone)
        {
            int c = 0;

            while (c < Tiles.Count && Tiles[c].AssociatedZone != zone)
            {
                c++;
            }

            if(c == Tiles.Count)
            {
                return null;
            }
            else
            {
                return Tiles[c];
            }

        }

		void CustomDestroy()
		{
            SpriteManager.Camera.X = 0;
            SpriteManager.Camera.Y = 0;

        }

        static void CustomLoadStaticContent(string contentManagerName)
        {


        }

	}
}
