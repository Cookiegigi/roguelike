using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

using FlatRedBall;
using FlatRedBall.Input;
using FlatRedBall.Instructions;
using FlatRedBall.AI.Pathfinding;
using FlatRedBall.Graphics.Animation;
using FlatRedBall.Graphics.Particle;
using FlatRedBall.Math.Geometry;
using FlatRedBall.Localization;
using Game.Entities.EntityType;
using Game.Entities;
using Game.Entities.ZoneEntity;
using Game.Entities.Debug;
using FlatRedBall.Math.Collision;
using Game.DataType.WorldType;
using Game.DataType.ZoneType;
using Game.DataType.Global;
using Game.Data.Global;
using Game.Entities.LayerEntity;

namespace Game.Screens
{
    /// <summary>
    /// Main screen which display zone
    /// </summary>
	public partial class ZoneScreen
	{
        private static int[] sizeOfZone;

        public static int[] SizeOfZone
        {
            get
            {
                return sizeOfZone;
            }

        }

        static ZoneScreen()
        {
            sizeOfZone = new int[2];
        }

        public World Zones { get; set; }

        private IPressableInput openMapKey;

        public int MarginH { get; set; } = 100;
        public int MarginV { get; set; } = 100;

        void CustomInitialize()
		{
            sizeOfZone = new int[2];
            sizeOfZone[0] = (int)(SpriteManager.Camera.AbsoluteRightXEdgeAt(0) - MarginH);
            sizeOfZone[1] = (int)(SpriteManager.Camera.AbsoluteTopYEdgeAt(0) - MarginV);

            Zones = GlobalWorld.CurrentWorld;

            if (DebugStat)
            {
                InitDebugStat();
            }
            
            InitZoneBoundsAndGates();
            CreateCollisionRelationShip();
            InitZoneFloor();
            InitZonePath();

            if (DebugZone)
            {
                InitDebugZone();
            }

            InitKeys();

        }

        private void InitZonePath()
        {
            PathLayerEntity layer = new PathLayerEntity(Zones.CurrentZone.Path);
            layer.X = 0;
            layer.Y = 0;

            layer.SpriteInstance.ScaleX = SizeOfZone[0];
            layer.SpriteInstance.ScaleY = SizeOfZone[1];

            LayersSprites.Add(layer);
        }

        private void InitZoneFloor()
        {
            FloorLayerEntity layer = new FloorLayerEntity(Zones.CurrentZone.Floor);
            layer.X = 0;
            layer.Y = 0;

            layer.SpriteInstance.ScaleX = SizeOfZone[0];
            layer.SpriteInstance.ScaleY = SizeOfZone[1];

            LayersSprites.Add(layer);
            

        }

        private void InitKeys()
        {
            openMapKey = InputManager.Keyboard.GetKey(KeysMap.Map["Open Map"]);
            
        }

        private void CreateCollisionRelationShip()
        {
            MovableVsBoundsCollisionRelationShip();
            MovablleVsGatesCollisionRelationShip();
        }

        private void MovablleVsGatesCollisionRelationShip()
        {
            var relationShip = CollisionManager.Self.CreateRelationship(PlayerInstance,
                ZoneGates);
            relationShip.CollisionOccurred += CollisionWithGateOccurred;
        }

        private void CollisionWithGateOccurred(MovableEntity arg1, Gate arg2)
        {
            bool open = arg2.Open;

            float movableMass = 0;
            float gateMass = 1;

            if (open)
            {
                int dir = ZoneGates.IndexOf(arg2);
                Zones.Move((CardinalPoint)dir);

                LayerChangeZone(Zones.CurrentZone);

                ChangePositionOfMovableWhenChangeRoom((CardinalPoint)((dir + 2) % 4));
            }
            else
            {
                arg1.CollideAgainstMove(arg2, movableMass, gateMass);
            }
        }

        private void LayerChangeZone(Zone currentZone)
        {
            LayersSprites[0].Layer = currentZone.Floor;
            LayersSprites[0].NeedUpdate = true;
            LayersSprites[1].Layer = currentZone.Path;
            LayersSprites[1].NeedUpdate = true;
        }

        private void ChangePositionOfMovableWhenChangeRoom(CardinalPoint entryDir)
        {
            float x = 0;
            float y = 0;

            float screenWidth = SpriteManager.Camera.AbsoluteRightXEdgeAt(0);
            float screenHeight = SpriteManager.Camera.AbsoluteTopYEdgeAt(0);

            float boundWidth = 100;
            float MovableRadius = PlayerInstance.CollisionCircleRadius;

            if (entryDir == CardinalPoint.North)
            {
                x = 0;
                y = screenHeight - boundWidth - MovableRadius;
            }
            else if(entryDir == CardinalPoint.East)
            {
                x = screenWidth - boundWidth - MovableRadius;
                y = 0;
            }
            else if(entryDir == CardinalPoint.South)
            {
                x = 0;
                y = -(screenHeight - boundWidth - MovableRadius);
            }
            else if(entryDir == CardinalPoint.West)
            {
                x = -(screenWidth - boundWidth - MovableRadius);
                y = 0;
            }

            PlayerInstance.X = x;
            PlayerInstance.Y = y;
        }

        private void MovableVsBoundsCollisionRelationShip()
        {
            var relationShip = CollisionManager.Self.CreateRelationship(PlayerInstance,
                ZoneBounds);
            float movableMass = 0;
            float boundsMass = 1;
            relationShip.SetMoveCollision(movableMass, boundsMass);
        }

        private void InitZoneBoundsAndGates()
        {
            float marginV = MarginV;
            float marginH = MarginH;

            float screenWidth = SpriteManager.Camera.AbsoluteRightXEdgeAt(0);
            float screenHeight = SpriteManager.Camera.AbsoluteTopYEdgeAt(0);

            //to be sure the movable entity doesn't go out of bounds with high velocity
            float safetyThickness = 1000;

            float doorWidth = 200;
            float doorSafetyWidth = 100;

            //sizeOfZone update

            SizeOfZone[0] = (int)(screenWidth - marginH);
            SizeOfZone[1] = (int)(screenHeight - marginV);

            // bounds

            //North - East
            ZoneBounds.Add(
                CreateZoneBound(
                (screenWidth-marginH + safetyThickness + doorWidth) / 2
                , screenHeight - marginV + safetyThickness/2
                , screenWidth - marginH + safetyThickness - doorWidth/2
                , safetyThickness
                , "NE")
                );

            //North-West
            ZoneBounds.Add(CreateZoneBound(
                -(screenWidth - marginH + safetyThickness + doorWidth) / 2
                , screenHeight - marginV + safetyThickness/2
                , screenWidth - marginH + safetyThickness - doorWidth/2
                , safetyThickness
                , "NW"));

            //South-East
            ZoneBounds.Add(CreateZoneBound(
                (screenWidth - marginH + safetyThickness + doorWidth) / 2
                , -(screenHeight - marginV + safetyThickness / 2)
                , screenWidth - marginH + safetyThickness - doorWidth/2
                , safetyThickness
                , "SE"));

            //South-West
            ZoneBounds.Add(CreateZoneBound(
                -(screenWidth - marginH + safetyThickness + doorWidth) / 2 
                , -(screenHeight - marginV + safetyThickness/2) 
                , screenWidth - marginH + safetyThickness - doorWidth/2
                , safetyThickness
                , "SW"));

            //East-North
            ZoneBounds.Add(CreateZoneBound(
                screenWidth-marginH + safetyThickness/2
                , (screenHeight - marginV + safetyThickness + doorWidth) / 2 
                , safetyThickness
                , screenHeight - marginV + safetyThickness - doorWidth/2
                , "EN"));

            //East-South
            ZoneBounds.Add(CreateZoneBound(
                screenWidth - marginH + safetyThickness/2
                , -(screenHeight - marginV + safetyThickness + doorWidth) / 2
                , safetyThickness
                , screenHeight - marginV + safetyThickness - doorWidth/2
                , "ES"));

            //West-North
            ZoneBounds.Add(CreateZoneBound(
                -(screenWidth - marginH + safetyThickness/2)
                , (screenHeight - marginV + +safetyThickness + doorWidth) / 2
                , safetyThickness
                , screenHeight - marginV +safetyThickness - doorWidth/2
                , "WN"));

            //West-South
            ZoneBounds.Add(CreateZoneBound(
                -(screenWidth - marginH + safetyThickness/2)
                , -(screenHeight - marginV +safetyThickness + doorWidth) / 2
                , safetyThickness
                , screenHeight - marginV + safetyThickness - doorWidth/2
                , "WS"));

            //Gate

            //North
            ZoneGates.Add(CreateZoneGate(
                0
                , screenHeight - marginV + safetyThickness / 2
                , doorWidth + doorSafetyWidth + marginH
                , safetyThickness
                , "N"
                ));

            // East
            ZoneGates.Add(CreateZoneGate(
                screenWidth - marginH + safetyThickness / 2
                , 0
                , safetyThickness
                , doorWidth + doorSafetyWidth + marginV
                , "N"
                ));

            //South
            ZoneGates.Add(CreateZoneGate(
                0
                , -(screenHeight - marginV + safetyThickness / 2)
                , doorWidth + doorSafetyWidth + marginH
                , safetyThickness
                , "N"
                ));

            //West
            ZoneGates.Add(CreateZoneGate(
                -(screenWidth - marginH + safetyThickness / 2)
                , 0
                , safetyThickness
                , doorWidth + doorSafetyWidth + marginV
                , "N"
                ));

        }

        private Gate CreateZoneGate(float x, float y, float width, float height, string name)
        {
            Gate entity = new Gate();

            entity.AxisAlignedRectangleInstance.Height = height;
            entity.AxisAlignedRectangleInstance.Width = width;

            return CreateZoneWallEntity(
                x, y, width, height, name
                , entity) as Gate;
        }

        private Bound CreateZoneBound(float x, float y
            , float width, float height, string name)
        {
            Bound entity = new Bound();

            entity.AxisAlignedRectangleInstance.Height = height;
            entity.AxisAlignedRectangleInstance.Width = width;

            return CreateZoneWallEntity(
                x, y, width, height, name
                , entity) as Bound;
        }

        private WallEntity CreateZoneWallEntity(float x, float y
            , float width, float height, string name
            , WallEntity entity)
        {
            entity.X = x;
            entity.Y = y;

            entity.Name = name;

            return entity;
        }

        private void InitDebugStat()
        {
            StatDebugHUD statHUD = new StatDebugHUD(StatTested, PlayerInstance);
            statHUD.X = SpriteManager.Camera.AbsoluteLeftXEdgeAt(0);
            statHUD.Y = SpriteManager.Camera.AbsoluteTopYEdgeAt(0);
            statHUD.Name = "StatHUDDebug";

            DebugHUDs.Add(statHUD);

        }

        private void InitDebugZone()
        {
            ZoneDebugHUD zoneHUD = new ZoneDebugHUD(Zones);
            zoneHUD.X = SpriteManager.Camera.AbsoluteLeftXEdgeAt(0); ;
            zoneHUD.Y = SpriteManager.Camera.AbsoluteTopYEdgeAt(0); ;
            zoneHUD.Name = "ZoneDebugHUD";

            DebugHUDs.Add(zoneHUD);
        }

        void CustomActivity(bool firstTimeCalled)
		{
            GateActivity();
            MapActivity();
		}

        private void MapActivity()
        {
            if (openMapKey.IsDown)
            {
                MoveToScreen(typeof(MapScreen));
            }
        }

        private void GateActivity()
        {
            for(int i = 0; i < ZoneGates.Count; i++)
            {
                if(ZoneGates[i].Open != Zones.CurrentZone.GatesState[i])
                {
                    ZoneGates[i].Open = Zones.CurrentZone.GatesState[i];
                }
            }
        }

        void CustomDestroy()
		{


		}

        static void CustomLoadStaticContent(string contentManagerName)
        {


        }

	}
}
