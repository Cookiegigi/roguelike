#if ANDROID || IOS || DESKTOP_GL
#define REQUIRES_PRIMARY_THREAD_LOADING
#endif
using Color = Microsoft.Xna.Framework.Color;
using System.Linq;
using FlatRedBall;
using System;
using System.Collections.Generic;
using System.Text;
using FlatRedBall.Math;
namespace Game.Screens
{
    public partial class ZoneScreen : FlatRedBall.Screens.Screen
    {
        #if DEBUG
        static bool HasBeenLoadedWithGlobalContentManager = false;
        #endif
        
        private FlatRedBall.Math.PositionedObjectList<Game.Entities.ZoneEntity.Bound> ZoneBounds;
        private FlatRedBall.Math.PositionedObjectList<Game.Entities.Debug.DebugHUD> DebugHUDs;
        private FlatRedBall.Math.PositionedObjectList<Game.Entities.ZoneEntity.Gate> ZoneGates;
        private FlatRedBall.Math.PositionedObjectList<Game.Entities.EntityType.LayerEntity> LayersSprites;
        private Game.Entities.Player PlayerInstance;
        public bool DebugStat = false;
        public string StatTested = "MovementSpeed";
        public bool DebugZone = true;
        public bool Pregenerate = true;
        public int PregenerateRadius;
        public ZoneScreen () 
        	: base ("ZoneScreen")
        {
        }
        public override void Initialize (bool addToManagers) 
        {
            LoadStaticContent(ContentManagerName);
            ZoneBounds = new FlatRedBall.Math.PositionedObjectList<Game.Entities.ZoneEntity.Bound>();
            ZoneBounds.Name = "ZoneBounds";
            DebugHUDs = new FlatRedBall.Math.PositionedObjectList<Game.Entities.Debug.DebugHUD>();
            DebugHUDs.Name = "DebugHUDs";
            ZoneGates = new FlatRedBall.Math.PositionedObjectList<Game.Entities.ZoneEntity.Gate>();
            ZoneGates.Name = "ZoneGates";
            LayersSprites = new FlatRedBall.Math.PositionedObjectList<Game.Entities.EntityType.LayerEntity>();
            LayersSprites.Name = "LayersSprites";
            PlayerInstance = new Game.Entities.Player(ContentManagerName, false);
            PlayerInstance.Name = "PlayerInstance";
            
            
            PostInitialize();
            base.Initialize(addToManagers);
            if (addToManagers)
            {
                AddToManagers();
            }
        }
        public override void AddToManagers () 
        {
            PlayerInstance.AddToManagers(mLayer);
            base.AddToManagers();
            AddToManagersBottomUp();
            CustomInitialize();
        }
        public override void Activity (bool firstTimeCalled) 
        {
            if (!IsPaused)
            {
                
                for (int i = ZoneBounds.Count - 1; i > -1; i--)
                {
                    if (i < ZoneBounds.Count)
                    {
                        // We do the extra if-check because activity could destroy any number of entities
                        ZoneBounds[i].Activity();
                    }
                }
                for (int i = DebugHUDs.Count - 1; i > -1; i--)
                {
                    if (i < DebugHUDs.Count)
                    {
                        // We do the extra if-check because activity could destroy any number of entities
                        DebugHUDs[i].Activity();
                    }
                }
                for (int i = ZoneGates.Count - 1; i > -1; i--)
                {
                    if (i < ZoneGates.Count)
                    {
                        // We do the extra if-check because activity could destroy any number of entities
                        ZoneGates[i].Activity();
                    }
                }
                for (int i = LayersSprites.Count - 1; i > -1; i--)
                {
                    if (i < LayersSprites.Count)
                    {
                        // We do the extra if-check because activity could destroy any number of entities
                        LayersSprites[i].Activity();
                    }
                }
                PlayerInstance.Activity();
            }
            else
            {
            }
            base.Activity(firstTimeCalled);
            if (!IsActivityFinished)
            {
                CustomActivity(firstTimeCalled);
            }
        }
        public override void Destroy () 
        {
            base.Destroy();
            
            ZoneBounds.MakeOneWay();
            DebugHUDs.MakeOneWay();
            ZoneGates.MakeOneWay();
            LayersSprites.MakeOneWay();
            for (int i = ZoneBounds.Count - 1; i > -1; i--)
            {
                ZoneBounds[i].Destroy();
            }
            for (int i = DebugHUDs.Count - 1; i > -1; i--)
            {
                DebugHUDs[i].Destroy();
            }
            for (int i = ZoneGates.Count - 1; i > -1; i--)
            {
                ZoneGates[i].Destroy();
            }
            for (int i = LayersSprites.Count - 1; i > -1; i--)
            {
                LayersSprites[i].Destroy();
            }
            if (PlayerInstance != null)
            {
                PlayerInstance.Destroy();
                PlayerInstance.Detach();
            }
            ZoneBounds.MakeTwoWay();
            DebugHUDs.MakeTwoWay();
            ZoneGates.MakeTwoWay();
            LayersSprites.MakeTwoWay();
            FlatRedBall.Math.Collision.CollisionManager.Self.Relationships.Clear();
            CustomDestroy();
        }
        public virtual void PostInitialize () 
        {
            bool oldShapeManagerSuppressAdd = FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue;
            FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue = true;
            FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue = oldShapeManagerSuppressAdd;
        }
        public virtual void AddToManagersBottomUp () 
        {
            CameraSetup.ResetCamera(SpriteManager.Camera);
            AssignCustomVariables(false);
        }
        public virtual void RemoveFromManagers () 
        {
            for (int i = ZoneBounds.Count - 1; i > -1; i--)
            {
                ZoneBounds[i].Destroy();
            }
            for (int i = DebugHUDs.Count - 1; i > -1; i--)
            {
                DebugHUDs[i].Destroy();
            }
            for (int i = ZoneGates.Count - 1; i > -1; i--)
            {
                ZoneGates[i].Destroy();
            }
            for (int i = LayersSprites.Count - 1; i > -1; i--)
            {
                LayersSprites[i].Destroy();
            }
            PlayerInstance.RemoveFromManagers();
        }
        public virtual void AssignCustomVariables (bool callOnContainedElements) 
        {
            if (callOnContainedElements)
            {
                PlayerInstance.AssignCustomVariables(true);
            }
            DebugStat = false;
            StatTested = "MovementSpeed";
            DebugZone = true;
            Pregenerate = true;
        }
        public virtual void ConvertToManuallyUpdated () 
        {
            for (int i = 0; i < ZoneBounds.Count; i++)
            {
                ZoneBounds[i].ConvertToManuallyUpdated();
            }
            for (int i = 0; i < DebugHUDs.Count; i++)
            {
                DebugHUDs[i].ConvertToManuallyUpdated();
            }
            for (int i = 0; i < ZoneGates.Count; i++)
            {
                ZoneGates[i].ConvertToManuallyUpdated();
            }
            for (int i = 0; i < LayersSprites.Count; i++)
            {
                LayersSprites[i].ConvertToManuallyUpdated();
            }
            PlayerInstance.ConvertToManuallyUpdated();
        }
        public static void LoadStaticContent (string contentManagerName) 
        {
            if (string.IsNullOrEmpty(contentManagerName))
            {
                throw new System.ArgumentException("contentManagerName cannot be empty or null");
            }
            #if DEBUG
            if (contentManagerName == FlatRedBall.FlatRedBallServices.GlobalContentManager)
            {
                HasBeenLoadedWithGlobalContentManager = true;
            }
            else if (HasBeenLoadedWithGlobalContentManager)
            {
                throw new System.Exception("This type has been loaded with a Global content manager, then loaded with a non-global.  This can lead to a lot of bugs");
            }
            #endif
            Game.Entities.Player.LoadStaticContent(contentManagerName);
            CustomLoadStaticContent(contentManagerName);
        }
        public override void PauseThisScreen () 
        {
            base.PauseThisScreen();
        }
        public override void UnpauseThisScreen () 
        {
            base.UnpauseThisScreen();
        }
        [System.Obsolete("Use GetFile instead")]
        public static object GetStaticMember (string memberName) 
        {
            return null;
        }
        public static object GetFile (string memberName) 
        {
            return null;
        }
        object GetMember (string memberName) 
        {
            return null;
        }
    }
}
