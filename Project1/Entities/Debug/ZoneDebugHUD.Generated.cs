#if ANDROID || IOS || DESKTOP_GL
#define REQUIRES_PRIMARY_THREAD_LOADING
#endif
using Color = Microsoft.Xna.Framework.Color;
using System.Linq;
using FlatRedBall.Graphics;
using FlatRedBall.Math;
using FlatRedBall;
using System;
using System.Collections.Generic;
using System.Text;
namespace Game.Entities.Debug
{
    public partial class ZoneDebugHUD : Game.Entities.Debug.DebugHUD, FlatRedBall.Graphics.IDestroyable
    {
        // This is made static so that static lazy-loaded content can access it.
        public static new string ContentManagerName
        {
            get
            {
                return Game.Entities.Debug.DebugHUD.ContentManagerName;
            }
            set
            {
                Game.Entities.Debug.DebugHUD.ContentManagerName = value;
            }
        }
        #if DEBUG
        static bool HasBeenLoadedWithGlobalContentManager = false;
        #endif
        static object mLockObject = new object();
        static System.Collections.Generic.List<string> mRegisteredUnloads = new System.Collections.Generic.List<string>();
        static System.Collections.Generic.List<string> LoadedContentManagers = new System.Collections.Generic.List<string>();
        
        private FlatRedBall.Graphics.Text NorthOpen;
        private FlatRedBall.Graphics.Text EastOpen;
        private FlatRedBall.Graphics.Text SouthOpen;
        private FlatRedBall.Graphics.Text WestOpen;
        private FlatRedBall.Graphics.Text Adress;
        private FlatRedBall.Graphics.Text BiomeText;
        public ZoneDebugHUD () 
        	: this(FlatRedBall.Screens.ScreenManager.CurrentScreen.ContentManagerName, true)
        {
        }
        public ZoneDebugHUD (string contentManagerName) 
        	: this(contentManagerName, true)
        {
        }
        public ZoneDebugHUD (string contentManagerName, bool addToManagers) 
        	: base(contentManagerName, addToManagers)
        {
            ContentManagerName = contentManagerName;
        }
        protected override void InitializeEntity (bool addToManagers) 
        {
            LoadStaticContent(ContentManagerName);
            NorthOpen = new FlatRedBall.Graphics.Text();
            NorthOpen.Name = "NorthOpen";
            EastOpen = new FlatRedBall.Graphics.Text();
            EastOpen.Name = "EastOpen";
            SouthOpen = new FlatRedBall.Graphics.Text();
            SouthOpen.Name = "SouthOpen";
            WestOpen = new FlatRedBall.Graphics.Text();
            WestOpen.Name = "WestOpen";
            Adress = new FlatRedBall.Graphics.Text();
            Adress.Name = "Adress";
            BiomeText = new FlatRedBall.Graphics.Text();
            BiomeText.Name = "BiomeText";
            
            base.InitializeEntity(addToManagers);
        }
        public override void ReAddToManagers (FlatRedBall.Graphics.Layer layerToAddTo) 
        {
            base.ReAddToManagers(layerToAddTo);
            FlatRedBall.Graphics.TextManager.AddToLayer(NorthOpen, LayerProvidedByContainer);
            if (NorthOpen.Font != null)
            {
                NorthOpen.SetPixelPerfectScale(LayerProvidedByContainer);
            }
            FlatRedBall.Graphics.TextManager.AddToLayer(EastOpen, LayerProvidedByContainer);
            if (EastOpen.Font != null)
            {
                EastOpen.SetPixelPerfectScale(LayerProvidedByContainer);
            }
            FlatRedBall.Graphics.TextManager.AddToLayer(SouthOpen, LayerProvidedByContainer);
            if (SouthOpen.Font != null)
            {
                SouthOpen.SetPixelPerfectScale(LayerProvidedByContainer);
            }
            FlatRedBall.Graphics.TextManager.AddToLayer(WestOpen, LayerProvidedByContainer);
            if (WestOpen.Font != null)
            {
                WestOpen.SetPixelPerfectScale(LayerProvidedByContainer);
            }
            FlatRedBall.Graphics.TextManager.AddToLayer(Adress, LayerProvidedByContainer);
            if (Adress.Font != null)
            {
                Adress.SetPixelPerfectScale(LayerProvidedByContainer);
            }
            FlatRedBall.Graphics.TextManager.AddToLayer(BiomeText, LayerProvidedByContainer);
            if (BiomeText.Font != null)
            {
                BiomeText.SetPixelPerfectScale(LayerProvidedByContainer);
            }
        }
        public override void AddToManagers (FlatRedBall.Graphics.Layer layerToAddTo) 
        {
            LayerProvidedByContainer = layerToAddTo;
            FlatRedBall.Graphics.TextManager.AddToLayer(NorthOpen, LayerProvidedByContainer);
            if (NorthOpen.Font != null)
            {
                NorthOpen.SetPixelPerfectScale(LayerProvidedByContainer);
            }
            FlatRedBall.Graphics.TextManager.AddToLayer(EastOpen, LayerProvidedByContainer);
            if (EastOpen.Font != null)
            {
                EastOpen.SetPixelPerfectScale(LayerProvidedByContainer);
            }
            FlatRedBall.Graphics.TextManager.AddToLayer(SouthOpen, LayerProvidedByContainer);
            if (SouthOpen.Font != null)
            {
                SouthOpen.SetPixelPerfectScale(LayerProvidedByContainer);
            }
            FlatRedBall.Graphics.TextManager.AddToLayer(WestOpen, LayerProvidedByContainer);
            if (WestOpen.Font != null)
            {
                WestOpen.SetPixelPerfectScale(LayerProvidedByContainer);
            }
            FlatRedBall.Graphics.TextManager.AddToLayer(Adress, LayerProvidedByContainer);
            if (Adress.Font != null)
            {
                Adress.SetPixelPerfectScale(LayerProvidedByContainer);
            }
            FlatRedBall.Graphics.TextManager.AddToLayer(BiomeText, LayerProvidedByContainer);
            if (BiomeText.Font != null)
            {
                BiomeText.SetPixelPerfectScale(LayerProvidedByContainer);
            }
            base.AddToManagers(layerToAddTo);
            CustomInitialize();
        }
        public override void Activity () 
        {
            base.Activity();
            
            CustomActivity();
        }
        public override void Destroy () 
        {
            base.Destroy();
            
            if (NorthOpen != null)
            {
                FlatRedBall.Graphics.TextManager.RemoveText(NorthOpen);
            }
            if (EastOpen != null)
            {
                FlatRedBall.Graphics.TextManager.RemoveText(EastOpen);
            }
            if (SouthOpen != null)
            {
                FlatRedBall.Graphics.TextManager.RemoveText(SouthOpen);
            }
            if (WestOpen != null)
            {
                FlatRedBall.Graphics.TextManager.RemoveText(WestOpen);
            }
            if (Adress != null)
            {
                FlatRedBall.Graphics.TextManager.RemoveText(Adress);
            }
            if (BiomeText != null)
            {
                FlatRedBall.Graphics.TextManager.RemoveText(BiomeText);
            }
            CustomDestroy();
        }
        public override void PostInitialize () 
        {
            bool oldShapeManagerSuppressAdd = FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue;
            FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue = true;
            base.PostInitialize();
            if (NorthOpen.Parent == null)
            {
                NorthOpen.CopyAbsoluteToRelative();
                NorthOpen.AttachTo(this, false);
            }
            if (NorthOpen.Parent == null)
            {
                NorthOpen.X = 50f;
            }
            else
            {
                NorthOpen.RelativeX = 50f;
            }
            if (NorthOpen.Parent == null)
            {
                NorthOpen.Y = -40f;
            }
            else
            {
                NorthOpen.RelativeY = -40f;
            }
            if (EastOpen.Parent == null)
            {
                EastOpen.CopyAbsoluteToRelative();
                EastOpen.AttachTo(this, false);
            }
            if (EastOpen.Parent == null)
            {
                EastOpen.X = 50f;
            }
            else
            {
                EastOpen.RelativeX = 50f;
            }
            if (EastOpen.Parent == null)
            {
                EastOpen.Y = -80f;
            }
            else
            {
                EastOpen.RelativeY = -80f;
            }
            if (SouthOpen.Parent == null)
            {
                SouthOpen.CopyAbsoluteToRelative();
                SouthOpen.AttachTo(this, false);
            }
            if (SouthOpen.Parent == null)
            {
                SouthOpen.X = 50f;
            }
            else
            {
                SouthOpen.RelativeX = 50f;
            }
            if (SouthOpen.Parent == null)
            {
                SouthOpen.Y = -120f;
            }
            else
            {
                SouthOpen.RelativeY = -120f;
            }
            if (WestOpen.Parent == null)
            {
                WestOpen.CopyAbsoluteToRelative();
                WestOpen.AttachTo(this, false);
            }
            if (WestOpen.Parent == null)
            {
                WestOpen.X = 50f;
            }
            else
            {
                WestOpen.RelativeX = 50f;
            }
            if (WestOpen.Parent == null)
            {
                WestOpen.Y = -160f;
            }
            else
            {
                WestOpen.RelativeY = -160f;
            }
            if (Adress.Parent == null)
            {
                Adress.CopyAbsoluteToRelative();
                Adress.AttachTo(this, false);
            }
            if (Adress.Parent == null)
            {
                Adress.X = 50f;
            }
            else
            {
                Adress.RelativeX = 50f;
            }
            if (Adress.Parent == null)
            {
                Adress.Y = -200f;
            }
            else
            {
                Adress.RelativeY = -200f;
            }
            if (BiomeText.Parent == null)
            {
                BiomeText.CopyAbsoluteToRelative();
                BiomeText.AttachTo(this, false);
            }
            if (BiomeText.Parent == null)
            {
                BiomeText.X = 50f;
            }
            else
            {
                BiomeText.RelativeX = 50f;
            }
            if (BiomeText.Parent == null)
            {
                BiomeText.Y = -240f;
            }
            else
            {
                BiomeText.RelativeY = -240f;
            }
            FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue = oldShapeManagerSuppressAdd;
        }
        public override void AddToManagersBottomUp (FlatRedBall.Graphics.Layer layerToAddTo) 
        {
            base.AddToManagersBottomUp(layerToAddTo);
        }
        public override void RemoveFromManagers () 
        {
            base.RemoveFromManagers();
            if (NorthOpen != null)
            {
                FlatRedBall.Graphics.TextManager.RemoveTextOneWay(NorthOpen);
            }
            if (EastOpen != null)
            {
                FlatRedBall.Graphics.TextManager.RemoveTextOneWay(EastOpen);
            }
            if (SouthOpen != null)
            {
                FlatRedBall.Graphics.TextManager.RemoveTextOneWay(SouthOpen);
            }
            if (WestOpen != null)
            {
                FlatRedBall.Graphics.TextManager.RemoveTextOneWay(WestOpen);
            }
            if (Adress != null)
            {
                FlatRedBall.Graphics.TextManager.RemoveTextOneWay(Adress);
            }
            if (BiomeText != null)
            {
                FlatRedBall.Graphics.TextManager.RemoveTextOneWay(BiomeText);
            }
        }
        public override void AssignCustomVariables (bool callOnContainedElements) 
        {
            base.AssignCustomVariables(callOnContainedElements);
            if (callOnContainedElements)
            {
            }
            if (NorthOpen.Parent == null)
            {
                NorthOpen.X = 50f;
            }
            else
            {
                NorthOpen.RelativeX = 50f;
            }
            if (NorthOpen.Parent == null)
            {
                NorthOpen.Y = -40f;
            }
            else
            {
                NorthOpen.RelativeY = -40f;
            }
            if (EastOpen.Parent == null)
            {
                EastOpen.X = 50f;
            }
            else
            {
                EastOpen.RelativeX = 50f;
            }
            if (EastOpen.Parent == null)
            {
                EastOpen.Y = -80f;
            }
            else
            {
                EastOpen.RelativeY = -80f;
            }
            if (SouthOpen.Parent == null)
            {
                SouthOpen.X = 50f;
            }
            else
            {
                SouthOpen.RelativeX = 50f;
            }
            if (SouthOpen.Parent == null)
            {
                SouthOpen.Y = -120f;
            }
            else
            {
                SouthOpen.RelativeY = -120f;
            }
            if (WestOpen.Parent == null)
            {
                WestOpen.X = 50f;
            }
            else
            {
                WestOpen.RelativeX = 50f;
            }
            if (WestOpen.Parent == null)
            {
                WestOpen.Y = -160f;
            }
            else
            {
                WestOpen.RelativeY = -160f;
            }
            if (Adress.Parent == null)
            {
                Adress.X = 50f;
            }
            else
            {
                Adress.RelativeX = 50f;
            }
            if (Adress.Parent == null)
            {
                Adress.Y = -200f;
            }
            else
            {
                Adress.RelativeY = -200f;
            }
            if (BiomeText.Parent == null)
            {
                BiomeText.X = 50f;
            }
            else
            {
                BiomeText.RelativeX = 50f;
            }
            if (BiomeText.Parent == null)
            {
                BiomeText.Y = -240f;
            }
            else
            {
                BiomeText.RelativeY = -240f;
            }
        }
        public override void ConvertToManuallyUpdated () 
        {
            base.ConvertToManuallyUpdated();
            this.ForceUpdateDependenciesDeep();
            FlatRedBall.SpriteManager.ConvertToManuallyUpdated(this);
            FlatRedBall.Graphics.TextManager.ConvertToManuallyUpdated(NorthOpen);
            FlatRedBall.Graphics.TextManager.ConvertToManuallyUpdated(EastOpen);
            FlatRedBall.Graphics.TextManager.ConvertToManuallyUpdated(SouthOpen);
            FlatRedBall.Graphics.TextManager.ConvertToManuallyUpdated(WestOpen);
            FlatRedBall.Graphics.TextManager.ConvertToManuallyUpdated(Adress);
            FlatRedBall.Graphics.TextManager.ConvertToManuallyUpdated(BiomeText);
        }
        public static new void LoadStaticContent (string contentManagerName) 
        {
            if (string.IsNullOrEmpty(contentManagerName))
            {
                throw new System.ArgumentException("contentManagerName cannot be empty or null");
            }
            ContentManagerName = contentManagerName;
            Game.Entities.Debug.DebugHUD.LoadStaticContent(contentManagerName);
            #if DEBUG
            if (contentManagerName == FlatRedBall.FlatRedBallServices.GlobalContentManager)
            {
                HasBeenLoadedWithGlobalContentManager = true;
            }
            else if (HasBeenLoadedWithGlobalContentManager)
            {
                throw new System.Exception("This type has been loaded with a Global content manager, then loaded with a non-global.  This can lead to a lot of bugs");
            }
            #endif
            bool registerUnload = false;
            if (LoadedContentManagers.Contains(contentManagerName) == false)
            {
                LoadedContentManagers.Add(contentManagerName);
                lock (mLockObject)
                {
                    if (!mRegisteredUnloads.Contains(ContentManagerName) && ContentManagerName != FlatRedBall.FlatRedBallServices.GlobalContentManager)
                    {
                        FlatRedBall.FlatRedBallServices.GetContentManagerByName(ContentManagerName).AddUnloadMethod("ZoneDebugHUDStaticUnload", UnloadStaticContent);
                        mRegisteredUnloads.Add(ContentManagerName);
                    }
                }
            }
            if (registerUnload && ContentManagerName != FlatRedBall.FlatRedBallServices.GlobalContentManager)
            {
                lock (mLockObject)
                {
                    if (!mRegisteredUnloads.Contains(ContentManagerName) && ContentManagerName != FlatRedBall.FlatRedBallServices.GlobalContentManager)
                    {
                        FlatRedBall.FlatRedBallServices.GetContentManagerByName(ContentManagerName).AddUnloadMethod("ZoneDebugHUDStaticUnload", UnloadStaticContent);
                        mRegisteredUnloads.Add(ContentManagerName);
                    }
                }
            }
            CustomLoadStaticContent(contentManagerName);
        }
        public static new void UnloadStaticContent () 
        {
            if (LoadedContentManagers.Count != 0)
            {
                LoadedContentManagers.RemoveAt(0);
                mRegisteredUnloads.RemoveAt(0);
            }
            if (LoadedContentManagers.Count == 0)
            {
            }
        }
        [System.Obsolete("Use GetFile instead")]
        public static new object GetStaticMember (string memberName) 
        {
            return null;
        }
        public static new object GetFile (string memberName) 
        {
            return null;
        }
        object GetMember (string memberName) 
        {
            return null;
        }
        public override void SetToIgnorePausing () 
        {
            base.SetToIgnorePausing();
            FlatRedBall.Instructions.InstructionManager.IgnorePausingFor(NorthOpen);
            FlatRedBall.Instructions.InstructionManager.IgnorePausingFor(EastOpen);
            FlatRedBall.Instructions.InstructionManager.IgnorePausingFor(SouthOpen);
            FlatRedBall.Instructions.InstructionManager.IgnorePausingFor(WestOpen);
            FlatRedBall.Instructions.InstructionManager.IgnorePausingFor(Adress);
            FlatRedBall.Instructions.InstructionManager.IgnorePausingFor(BiomeText);
        }
        public override void MoveToLayer (FlatRedBall.Graphics.Layer layerToMoveTo) 
        {
            var layerToRemoveFrom = LayerProvidedByContainer; // assign before calling base so removal is not impacted by base call
            base.MoveToLayer(layerToMoveTo);
            if (layerToRemoveFrom != null)
            {
                layerToRemoveFrom.Remove(NorthOpen);
            }
            FlatRedBall.Graphics.TextManager.AddToLayer(NorthOpen, layerToMoveTo);
            if (layerToRemoveFrom != null)
            {
                layerToRemoveFrom.Remove(EastOpen);
            }
            FlatRedBall.Graphics.TextManager.AddToLayer(EastOpen, layerToMoveTo);
            if (layerToRemoveFrom != null)
            {
                layerToRemoveFrom.Remove(SouthOpen);
            }
            FlatRedBall.Graphics.TextManager.AddToLayer(SouthOpen, layerToMoveTo);
            if (layerToRemoveFrom != null)
            {
                layerToRemoveFrom.Remove(WestOpen);
            }
            FlatRedBall.Graphics.TextManager.AddToLayer(WestOpen, layerToMoveTo);
            if (layerToRemoveFrom != null)
            {
                layerToRemoveFrom.Remove(Adress);
            }
            FlatRedBall.Graphics.TextManager.AddToLayer(Adress, layerToMoveTo);
            if (layerToRemoveFrom != null)
            {
                layerToRemoveFrom.Remove(BiomeText);
            }
            FlatRedBall.Graphics.TextManager.AddToLayer(BiomeText, layerToMoveTo);
        }
    }
}
