using System;
using System.Collections.Generic;
using System.Text;
using FlatRedBall;
using FlatRedBall.Input;
using FlatRedBall.Instructions;
using FlatRedBall.AI.Pathfinding;
using FlatRedBall.Graphics.Animation;
using FlatRedBall.Graphics.Particle;
using FlatRedBall.Math.Geometry;
using Game.Entities.EntityType;

namespace Game.Entities.Debug
{
	public partial class StatDebugHUD
	{
        private IPressableInput increase;
        private IPressableInput decrease;

        public MovableEntity Player { get; set; }
        public string StatTested { get; set; }

        public StatDebugHUD(string statTested, MovableEntity player) : this()
        {
            Player = player;
            StatTested = statTested;
            StatName.DisplayText = StatTested;
        }

        /// <summary>
        /// Initialization logic which is execute only one time for this Entity (unless the Entity is pooled).
        /// This method is called when the Entity is added to managers. Entities which are instantiated but not
        /// added to managers will not have this method called.
        /// </summary>
		private void CustomInitialize()
		{
            increase = InputManager.Keyboard.GetKey(Data.Global.KeysMap.Map["+"]);
            decrease = InputManager.Keyboard.GetKey(Data.Global.KeysMap.Map["-"]);
        }

		private void CustomActivity()
		{
            StatValueDisplayText = Player.Stats[StatTested];

            if (increase.IsDown)
            {
                Player.Stats[StatTested] += 10;
            }
            else if (decrease.IsDown)
            {
                Player.Stats[StatTested] -= 10;
            }
        }

		private void CustomDestroy()
		{


		}

        private static void CustomLoadStaticContent(string contentManagerName)
        {


        }
	}
}
