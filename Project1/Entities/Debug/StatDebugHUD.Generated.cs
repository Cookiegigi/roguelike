#if ANDROID || IOS || DESKTOP_GL
#define REQUIRES_PRIMARY_THREAD_LOADING
#endif
using Color = Microsoft.Xna.Framework.Color;
using System.Linq;
using FlatRedBall.Graphics;
using FlatRedBall.Math;
using FlatRedBall;
using System;
using System.Collections.Generic;
using System.Text;
namespace Game.Entities.Debug
{
    public partial class StatDebugHUD : Game.Entities.Debug.DebugHUD, FlatRedBall.Graphics.IDestroyable
    {
        // This is made static so that static lazy-loaded content can access it.
        public static new string ContentManagerName
        {
            get
            {
                return Game.Entities.Debug.DebugHUD.ContentManagerName;
            }
            set
            {
                Game.Entities.Debug.DebugHUD.ContentManagerName = value;
            }
        }
        #if DEBUG
        static bool HasBeenLoadedWithGlobalContentManager = false;
        #endif
        static object mLockObject = new object();
        static System.Collections.Generic.List<string> mRegisteredUnloads = new System.Collections.Generic.List<string>();
        static System.Collections.Generic.List<string> LoadedContentManagers = new System.Collections.Generic.List<string>();
        
        private FlatRedBall.Graphics.Text StatName;
        private FlatRedBall.Graphics.Text StatValue;
        public float StatValueDisplayText
        {
            get
            {
                return float.Parse(StatValue.DisplayText);
            }
            set
            {
                StatValue.DisplayText = value.ToString();
            }
        }
        public StatDebugHUD () 
        	: this(FlatRedBall.Screens.ScreenManager.CurrentScreen.ContentManagerName, true)
        {
        }
        public StatDebugHUD (string contentManagerName) 
        	: this(contentManagerName, true)
        {
        }
        public StatDebugHUD (string contentManagerName, bool addToManagers) 
        	: base(contentManagerName, addToManagers)
        {
            ContentManagerName = contentManagerName;
        }
        protected override void InitializeEntity (bool addToManagers) 
        {
            LoadStaticContent(ContentManagerName);
            StatName = new FlatRedBall.Graphics.Text();
            StatName.Name = "StatName";
            StatValue = new FlatRedBall.Graphics.Text();
            StatValue.Name = "StatValue";
            
            base.InitializeEntity(addToManagers);
        }
        public override void ReAddToManagers (FlatRedBall.Graphics.Layer layerToAddTo) 
        {
            base.ReAddToManagers(layerToAddTo);
            FlatRedBall.Graphics.TextManager.AddToLayer(StatName, LayerProvidedByContainer);
            if (StatName.Font != null)
            {
                StatName.SetPixelPerfectScale(LayerProvidedByContainer);
            }
            FlatRedBall.Graphics.TextManager.AddToLayer(StatValue, LayerProvidedByContainer);
            if (StatValue.Font != null)
            {
                StatValue.SetPixelPerfectScale(LayerProvidedByContainer);
            }
        }
        public override void AddToManagers (FlatRedBall.Graphics.Layer layerToAddTo) 
        {
            LayerProvidedByContainer = layerToAddTo;
            FlatRedBall.Graphics.TextManager.AddToLayer(StatName, LayerProvidedByContainer);
            if (StatName.Font != null)
            {
                StatName.SetPixelPerfectScale(LayerProvidedByContainer);
            }
            FlatRedBall.Graphics.TextManager.AddToLayer(StatValue, LayerProvidedByContainer);
            if (StatValue.Font != null)
            {
                StatValue.SetPixelPerfectScale(LayerProvidedByContainer);
            }
            base.AddToManagers(layerToAddTo);
            CustomInitialize();
        }
        public override void Activity () 
        {
            base.Activity();
            
            CustomActivity();
        }
        public override void Destroy () 
        {
            base.Destroy();
            
            if (StatName != null)
            {
                FlatRedBall.Graphics.TextManager.RemoveText(StatName);
            }
            if (StatValue != null)
            {
                FlatRedBall.Graphics.TextManager.RemoveText(StatValue);
            }
            CustomDestroy();
        }
        public override void PostInitialize () 
        {
            bool oldShapeManagerSuppressAdd = FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue;
            FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue = true;
            base.PostInitialize();
            if (StatName.Parent == null)
            {
                StatName.CopyAbsoluteToRelative();
                StatName.AttachTo(this, false);
            }
            if (StatName.Parent == null)
            {
                StatName.X = 50f;
            }
            else
            {
                StatName.RelativeX = 50f;
            }
            if (StatName.Parent == null)
            {
                StatName.Y = -40f;
            }
            else
            {
                StatName.RelativeY = -40f;
            }
            StatName.AdjustPositionForPixelPerfectDrawing = true;
            if (StatValue.Parent == null)
            {
                StatValue.CopyAbsoluteToRelative();
                StatValue.AttachTo(this, false);
            }
            if (StatValue.Parent == null)
            {
                StatValue.X = 50f;
            }
            else
            {
                StatValue.RelativeX = 50f;
            }
            if (StatValue.Parent == null)
            {
                StatValue.Y = -80f;
            }
            else
            {
                StatValue.RelativeY = -80f;
            }
            StatValue.AdjustPositionForPixelPerfectDrawing = true;
            FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue = oldShapeManagerSuppressAdd;
        }
        public override void AddToManagersBottomUp (FlatRedBall.Graphics.Layer layerToAddTo) 
        {
            base.AddToManagersBottomUp(layerToAddTo);
        }
        public override void RemoveFromManagers () 
        {
            base.RemoveFromManagers();
            if (StatName != null)
            {
                FlatRedBall.Graphics.TextManager.RemoveTextOneWay(StatName);
            }
            if (StatValue != null)
            {
                FlatRedBall.Graphics.TextManager.RemoveTextOneWay(StatValue);
            }
        }
        public override void AssignCustomVariables (bool callOnContainedElements) 
        {
            base.AssignCustomVariables(callOnContainedElements);
            if (callOnContainedElements)
            {
            }
            if (StatName.Parent == null)
            {
                StatName.X = 50f;
            }
            else
            {
                StatName.RelativeX = 50f;
            }
            if (StatName.Parent == null)
            {
                StatName.Y = -40f;
            }
            else
            {
                StatName.RelativeY = -40f;
            }
            StatName.AdjustPositionForPixelPerfectDrawing = true;
            if (StatValue.Parent == null)
            {
                StatValue.X = 50f;
            }
            else
            {
                StatValue.RelativeX = 50f;
            }
            if (StatValue.Parent == null)
            {
                StatValue.Y = -80f;
            }
            else
            {
                StatValue.RelativeY = -80f;
            }
            StatValue.AdjustPositionForPixelPerfectDrawing = true;
            StatValueDisplayText = 0f;
        }
        public override void ConvertToManuallyUpdated () 
        {
            base.ConvertToManuallyUpdated();
            this.ForceUpdateDependenciesDeep();
            FlatRedBall.SpriteManager.ConvertToManuallyUpdated(this);
            FlatRedBall.Graphics.TextManager.ConvertToManuallyUpdated(StatName);
            FlatRedBall.Graphics.TextManager.ConvertToManuallyUpdated(StatValue);
        }
        public static new void LoadStaticContent (string contentManagerName) 
        {
            if (string.IsNullOrEmpty(contentManagerName))
            {
                throw new System.ArgumentException("contentManagerName cannot be empty or null");
            }
            ContentManagerName = contentManagerName;
            Game.Entities.Debug.DebugHUD.LoadStaticContent(contentManagerName);
            #if DEBUG
            if (contentManagerName == FlatRedBall.FlatRedBallServices.GlobalContentManager)
            {
                HasBeenLoadedWithGlobalContentManager = true;
            }
            else if (HasBeenLoadedWithGlobalContentManager)
            {
                throw new System.Exception("This type has been loaded with a Global content manager, then loaded with a non-global.  This can lead to a lot of bugs");
            }
            #endif
            bool registerUnload = false;
            if (LoadedContentManagers.Contains(contentManagerName) == false)
            {
                LoadedContentManagers.Add(contentManagerName);
                lock (mLockObject)
                {
                    if (!mRegisteredUnloads.Contains(ContentManagerName) && ContentManagerName != FlatRedBall.FlatRedBallServices.GlobalContentManager)
                    {
                        FlatRedBall.FlatRedBallServices.GetContentManagerByName(ContentManagerName).AddUnloadMethod("StatDebugHUDStaticUnload", UnloadStaticContent);
                        mRegisteredUnloads.Add(ContentManagerName);
                    }
                }
            }
            if (registerUnload && ContentManagerName != FlatRedBall.FlatRedBallServices.GlobalContentManager)
            {
                lock (mLockObject)
                {
                    if (!mRegisteredUnloads.Contains(ContentManagerName) && ContentManagerName != FlatRedBall.FlatRedBallServices.GlobalContentManager)
                    {
                        FlatRedBall.FlatRedBallServices.GetContentManagerByName(ContentManagerName).AddUnloadMethod("StatDebugHUDStaticUnload", UnloadStaticContent);
                        mRegisteredUnloads.Add(ContentManagerName);
                    }
                }
            }
            CustomLoadStaticContent(contentManagerName);
        }
        public static new void UnloadStaticContent () 
        {
            if (LoadedContentManagers.Count != 0)
            {
                LoadedContentManagers.RemoveAt(0);
                mRegisteredUnloads.RemoveAt(0);
            }
            if (LoadedContentManagers.Count == 0)
            {
            }
        }
        [System.Obsolete("Use GetFile instead")]
        public static new object GetStaticMember (string memberName) 
        {
            return null;
        }
        public static new object GetFile (string memberName) 
        {
            return null;
        }
        object GetMember (string memberName) 
        {
            return null;
        }
        public override void SetToIgnorePausing () 
        {
            base.SetToIgnorePausing();
            FlatRedBall.Instructions.InstructionManager.IgnorePausingFor(StatName);
            FlatRedBall.Instructions.InstructionManager.IgnorePausingFor(StatValue);
        }
        public override void MoveToLayer (FlatRedBall.Graphics.Layer layerToMoveTo) 
        {
            var layerToRemoveFrom = LayerProvidedByContainer; // assign before calling base so removal is not impacted by base call
            base.MoveToLayer(layerToMoveTo);
            if (layerToRemoveFrom != null)
            {
                layerToRemoveFrom.Remove(StatName);
            }
            FlatRedBall.Graphics.TextManager.AddToLayer(StatName, layerToMoveTo);
            if (layerToRemoveFrom != null)
            {
                layerToRemoveFrom.Remove(StatValue);
            }
            FlatRedBall.Graphics.TextManager.AddToLayer(StatValue, layerToMoveTo);
        }
    }
}
