using System;
using System.Collections.Generic;
using System.Text;
using FlatRedBall;
using FlatRedBall.Input;
using FlatRedBall.Instructions;
using FlatRedBall.AI.Pathfinding;
using FlatRedBall.Graphics.Animation;
using FlatRedBall.Graphics.Particle;
using FlatRedBall.Math.Geometry;
using Game.Entities.ZoneEntity;
using FlatRedBall.Math;
using Game.DataType.ZoneType;
using Game.DataType.Global;
using Game.DataType.WorldType;

namespace Game.Entities.Debug
{
	public partial class ZoneDebugHUD
	{
        private IPressableInput northInput;
        private IPressableInput eastInput;
        private IPressableInput southInput;
        private IPressableInput westInput;

        public World Zones { get; set; }

        public ZoneDebugHUD(World zones) : this()
        {
            Zones = zones;
        }

        /// <summary>
        /// Initialization logic which is execute only one time for this Entity (unless the Entity is pooled).
        /// This method is called when the Entity is added to managers. Entities which are instantiated but not
        /// added to managers will not have this method called.
        /// </summary>
		private void CustomInitialize()
		{
            northInput = InputManager.Keyboard.GetKey(Data.Global.KeysMap.Map["North"]);
            southInput = InputManager.Keyboard.GetKey(Data.Global.KeysMap.Map["South"]);
            eastInput = InputManager.Keyboard.GetKey(Data.Global.KeysMap.Map["East"]);
            westInput = InputManager.Keyboard.GetKey(Data.Global.KeysMap.Map["West"]);
        }

		private void CustomActivity()
		{
            Adress.DisplayText = Zones.CurrentZone.ZoneAdress.ToString() ;

            BiomeText.DisplayText = Data.Global.GlobalWorld.CurrentWorld.Seed.ToString();

            bool[] gateStates = Zones.CurrentZone.GatesState;

            NorthOpen.DisplayText = $"North Gate : {gateStates[(int)CardinalPoint.North]}";
            SouthOpen.DisplayText = $"South Gate : {gateStates[(int)CardinalPoint.South]}";
            EastOpen.DisplayText = $"East Gate : {gateStates[(int)CardinalPoint.East]}";
            WestOpen.DisplayText = $"West Gate : {gateStates[(int)CardinalPoint.West]}";

            if (northInput.WasJustPressed)
            {
                gateStates[(int)CardinalPoint.North] = !gateStates[(int)CardinalPoint.North];
            }
            else if (southInput.WasJustPressed)
            {
                gateStates[(int)CardinalPoint.South] = !gateStates[(int)CardinalPoint.South];
            }
            else if (eastInput.WasJustPressed)
            {
                gateStates[(int)CardinalPoint.East] = !gateStates[(int)CardinalPoint.East];
            }
            else if (westInput.WasJustPressed)
            {
                gateStates[(int)CardinalPoint.West] = !gateStates[(int)CardinalPoint.West];
            }



		}

		private void CustomDestroy()
		{


		}

        private static void CustomLoadStaticContent(string contentManagerName)
        {


        }
	}
}
