using System;
using System.Collections.Generic;
using System.Text;
using FlatRedBall;
using FlatRedBall.Input;
using FlatRedBall.Instructions;
using FlatRedBall.AI.Pathfinding;
using FlatRedBall.Graphics.Animation;
using FlatRedBall.Graphics.Particle;
using FlatRedBall.Math.Geometry;
using Game.DataType.ZoneType.Layers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Utility.ToolBox;

namespace Game.Entities.EntityType
{
    /// <summary>
    /// Mother of layer entity
    /// Display a layer
    /// </summary>
	public partial class LayerEntity
	{
        protected Layer layer;

        public Layer Layer
        {
            get
            {
                return layer;
            }
            set
            {
                layer = value;
            }
        }

        public bool NeedUpdate { get; set; }

        public LayerEntity(Layer layer) : this()
        {
            this.layer = layer;
        }

        /// <summary>
        /// Initialization logic which is execute only one time for this Entity (unless the Entity is pooled).
        /// This method is called when the Entity is added to managers. Entities which are instantiated but not
        /// added to managers will not have this method called.
        /// </summary>
		private void CustomInitialize()
        {

            NeedUpdate = true;

		}

        private void UpdateTexture()
        {
            Color[,] colors2D = layer.Colors;
            Color[] colors1D = ArrayToolBox.Convert2DTo1D<Color>(colors2D);


            if(this.SpriteInstance.Texture == null)
            {
                this.SpriteInstance.Texture = new Texture2D(FlatRedBallServices.GraphicsDevice,
                    colors2D.GetLength(0), colors2D.GetLength(1));
            }
            this.SpriteInstance.Texture.SetData(0
                , new Rectangle(0, 0, colors2D.GetLength(0), colors2D.GetLength(1))
                , colors1D
                , 0
                , colors2D.GetLength(0) * colors2D.GetLength(1));
        }

		private void CustomActivity()
		{
            if (NeedUpdate)
            {
                UpdateTexture();
                NeedUpdate = false;
            }

		}

		private void CustomDestroy()
		{


		}

        private static void CustomLoadStaticContent(string contentManagerName)
        {


        }
	}
}
