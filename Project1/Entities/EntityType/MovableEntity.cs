using System;
using System.Collections.Generic;
using System.Text;
using FlatRedBall;
using FlatRedBall.Input;
using FlatRedBall.Instructions;
using FlatRedBall.AI.Pathfinding;
using FlatRedBall.Graphics.Animation;
using FlatRedBall.Graphics.Particle;
using FlatRedBall.Math.Geometry;

namespace Game.Entities.EntityType
{
    /// <summary>
    /// Mother of all entity movable by player
    /// </summary>
	public partial class MovableEntity
	{
        public I2DInput Movement { get; private set; }


        /// <summary>
        /// Initialization logic which is execute only one time for this Entity (unless the Entity is pooled).
        /// This method is called when the Entity is added to managers. Entities which are instantiated but not
        /// added to managers will not have this method called.
        /// </summary>
		private void CustomInitialize()
		{
            Movement = InputManager.Keyboard.Get2DInput
                (
                Data.Global.KeysMap.Map["Left"],
                Data.Global.KeysMap.Map["Right"],
                Data.Global.KeysMap.Map["Up"],
                Data.Global.KeysMap.Map["Down"]
                );

            if (!Stats.Dictionnary.ContainsKey("MovementSpeed"))
            {
                Stats["MovementSpeed"] = 700;
                Stats.Save();
            }

		}

		private void CustomActivity()
		{
            MoveActivity();

		}

        private void MoveActivity()
        {
            XVelocity = Movement.X * Stats["MovementSpeed"];
            YVelocity = Movement.Y * Stats["MovementSpeed"];
        }

        private void CustomDestroy()
		{
            Stats.Save();

		}

        private static void CustomLoadStaticContent(string contentManagerName)
        {


        }


	}
}
