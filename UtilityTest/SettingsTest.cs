﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Utility.DataType;

namespace UtilityTest
{
    [TestClass]
    public class SettingsTest
    {
        [TestMethod]
        public void SettingsLoadSave()
        {
            GameData<double> settings = new GameData<double>("settings_test.xml");

            settings["test"] = 1;
            settings["test1"] = 2;
            settings["test2"] = 3;
            settings["test3"] = 4;

            settings.Save();

            GameData<double> settings2 = new GameData<double>("settings_test.xml");

            Assert.IsTrue(settings2.Dictionnary.ContainsKey("test2"));
            Assert.IsTrue(settings2["test2"] == 3);
        }
    }
}
