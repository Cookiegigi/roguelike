﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Utility.DataType;

namespace UtilityTest
{
    [TestClass]
    public class SerializableDictionnaryTest
    {
        [TestMethod]
        public void SaveLoadTest()
        {
            SerializableDictionnary<string, string> dico = new SerializableDictionnary<string, string>();

            dico["1"] = "1";
            dico["2"] = "2";
            dico["3"] = "3";
            dico["4"] = "4";
            dico["5"] = "5";

            dico.Save("dico.xml");

            SerializableDictionnary<string, string> dico2 = SerializableDictionnary<string, string>
                .Load("dico.xml");


            Assert.IsTrue(dico2.ContainsKey("2"));
            Assert.IsTrue(dico2["2"] == "2");
        }
    }
}
