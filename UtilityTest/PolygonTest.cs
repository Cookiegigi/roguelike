﻿using System;
using System.Collections.Generic;
using DelaunayVoronoi;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UtilityTest
{
    [TestClass]
    public class PolygonTest
    {
        [TestMethod]
        public void IsCompleteTest()
        {
            Point p1, p2, p3;
            Edge e1, e2, e3;
            Polygon pol;

            p1 = new Point(0, 0);
            p2 = new Point(1, 1);
            p3 = new Point(2, 2);

            e1 = new Edge(p1, p2);
            e2 = new Edge(p2, p3);
            e3 = new Edge(p3, p1);

            pol = new Polygon();
            pol.Edges.Add(e1);
            pol.Edges.Add(e2);
            pol.Edges.Add(e3);

            Assert.IsTrue(pol.IsComplete());

            p1 = new Point(0, 0);
            p2 = new Point(1, 1);
            p3 = new Point(2, 2);

            e1 = new Edge(p1, p2);
            e3 = new Edge(p3, p1);

            pol = new Polygon();
            pol.Edges.Add(e1);
            pol.Edges.Add(e3);

            Assert.IsFalse(pol.IsComplete());
        }

        [TestMethod]
        public void EqualTest()
        {
            Point p1, p2, p3, p4 ;
            Edge e1, e2, e3, e4;
            Polygon pol, pol2, pol3, pol4;

            p1 = new Point(0, 0);
            p2 = new Point(1, 1);
            p3 = new Point(2, 2);
            p4 = new Point(3, 3);

            e1 = new Edge(p1, p2);
            e2 = new Edge(p2, p3);
            e3 = new Edge(p3, p1);
            e4 = new Edge(p3, p4);

            pol = new Polygon();
            pol.Edges.Add(e1);
            pol.Edges.Add(e2);
            pol.Edges.Add(e3);

            pol2 = new Polygon();
            pol2.Edges.Add(e1);
            pol2.Edges.Add(e2);
            pol2.Edges.Add(e3);

            Assert.IsTrue(pol.Equals(pol2));

            pol3 = new Polygon();
            pol3.Edges.Add(e1);
            pol3.Edges.Add(e2);

            Assert.IsFalse(pol.Equals(pol3));

            pol4 = new Polygon();
            pol4.Edges.Add(e1);
            pol4.Edges.Add(e2);
            pol4.Edges.Add(e4);

            Assert.IsFalse(pol.Equals(pol4));
        }
    }
}
