﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Utility.ToolBox;

namespace UtilityTest
{
    [TestClass]
    public class MathToolBoxTest
    {
        [TestMethod]
        public void TestSpiral()
        {
            List<int[]> coord = new List<int[]>();

            for(int i = 1; i <= 10; i++)
            {
                coord.Add(MathToolBox.GetSpiralCoord(i));
            }

            Assert.IsTrue(coord[4][0] == -1);
            Assert.IsTrue(coord[4][1] == 0);


        }

        [TestMethod]
        public void TestConvolution()
        {
            double[,] array = new double[,]
            {
                { 1, 1, 1, 1, 1 },
                { 1, 1, 1, 1, 1 },
                { 1, 1, 1, 1, 1 },
                { 1, 1, 1, 1, 1 },
                { 1, 1, 1, 1, 1 }
            };
            double[,] kernel = new double[,]
            {
                {0, 0.25, 0 },
                {0.25, 0.25, 0.25 },
                {0, 0.25, 0 }
            };

            double[,] result = MathToolBox.ConvulutionProduct(array, kernel);
        }
    }
}
